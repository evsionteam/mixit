=== Ecster Pay for WooCommerce ===
Contributors: krokedil, niklashogefjord, slobodanmanic
Tags: ecommerce, e-commerce, woocommerce, ecster
Requires at least: 4.5
Tested up to: 4.7
Requires WooCommerce at least: 2.5
Tested WooCommerce up to: 2.6.8
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Stable tag: 1.3.3

Ecster Pay for WooCommerce is a plugin that extends WooCommerce, allowing you to take payments via Ecster.


== DESCRIPTION ==
Ecster Pay is a new e-commerce checkout solution that gives you more than the individual purchase. The checkout is built to provide more repeat customers and an increased average order value.

To get started with Ecster Pay you need to [sign up](https://www.ecster.se/foretag/ehandel/ecster-pay/bestall-ecster-pay) for an account.

More information on how to get started can be found in the [plugin documentation](http://docs.krokedil.com/se/documentation/ecster-pay-woocommerce/).


== INSTALLATION	 ==
1. Download and unzip the latest release zip file.
2. If you use the WordPress plugin uploader to install this plugin skip to step 4.
3. Upload the entire plugin directory to your /wp-content/plugins/ directory.
4. Activate the plugin through the 'Plugins' menu in WordPress Administration.
5. Go WooCommerce Settings --> Payment Gateways and configure your Ecster Pay settings.
6. Read more about the configuration process in the [plugin documentation](http://docs.krokedil.com/se/documentation/ecster-pay-woocommerce/).


== CHANGELOG ==

= 2017.01.03  - version 1.3.3 =
* Fix			- Better handling of different name formats returned in OSN callback from Ecster. Ecster return names in different formats depending on how the customer authenticates.
* Fix			- Fixes so addresses & internal/external reference is updated/stored correctly in OSN callback.

= 2017.01.02 	- version 1.3.2 =
* Update		- Remove changing of WC order status to 'Failed' on onPaymentDenied() & onPaymentFailure() events from Ecster. These events does not mean that the entire order has failed, just that one payment method has been denied in the Ecster iframe.
* Update		- Added setting for overriding 'Select another payment method' button text.
* Update		- Translation updates.
* Update		- Added link to documentation in settings page.

= 2016.12.20    - version 1.3.1 =
* Fix           - Fixes checkout issue when store is only selling to one country.

= 2016.12.19  	- version 1.3 =
* Update        - Improved handling of Ecster API response.
* Fix           - Stores billing and shipping address into order in OSN listener callback.
* Fix           - Sends correct SKU to Ecster if product is variation.
* Fix           - Changes how coupons are sent to Ecster (to each order line, instead of as separate order line).

= 2016.12.17  	- version 1.2.1 =
* Fix       	- Fixed Internet Explorer bug where redirect to order confirmation page (on onPaymentSuccess) didn’t work.
* Update		- Added logging in OSN listener (server to server callback from Ecster to WooCommerce).

= 2016.12.13    - version 1.2 =
* Fix           - Fixes error with WooCommerce order not being created in some cases
* Update        - Adds compatibility with Sequential Order Numbers and Sequential Order Numbers Pro.
* Update        - Sends WooCommerce order number to Ecster as externalReference.
* Update        - Adds product variations to Ecster.

= 2016.11.17  	- version 1.1.1 =
* Fix           - Fixes the issue with some Ecster invoice fee not being added to some orders.

= 2016.10.20  	- version 1.1 =
* Fix       	- Changes how invoice fee is added to WooCommerce order.
* Fix       	- Improves phone number handling when "Ange uppgifter själv" is used.

= 2016.09.25	- version 1.0 =
* Initial release.