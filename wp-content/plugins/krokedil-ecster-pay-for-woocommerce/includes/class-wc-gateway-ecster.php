<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Gateway_Ecster class.
 *
 * @extends WC_Payment_Gateway
 */
class WC_Gateway_Ecster extends WC_Payment_Gateway {

	/** @var string Ecster API username. */
	public $username;

	/** @var string Ecster API password. */
	public $password;

	/** @var boolean Ecster API testmode. */
	public $testmode;

	/** @var boolean Ecster debug logging. */
	public $logging;

	/** @var WC_Logger Logger instance */
	public static $log = false;

	private $allowed_tax_rates = array( 0, 6, 12, 25 );

	/**
	 * WC_Gateway_Ecster constructor.
	 */
	public function __construct() {
		$this->id                 = 'ecster';
		$this->method_title       = __( 'Ecster Pay', 'krokedil-ecster-pay-for-woocommerce' );
		$this->method_description = __( 'Ecster Pay description', 'krokedil-ecster-pay-for-woocommerce' );
		$this->method_description = sprintf( __( 'Documentation <a href="%s" target="_blank">can be found here</a>.', 'krokedil-ecster-pay-for-woocommerce' ), 'http://docs.krokedil.com/se/documentation/ecster-pay-woocommerce/' );
		$this->has_fields         = true;
		$this->supports           = array( 'products' );
		// Load the form fields.
		$this->init_form_fields();
		// Load the settings.
		$this->init_settings();
		// Get setting values.
		$this->title       					= $this->get_option( 'title' );
		$this->description 					= $this->get_option( 'description' );
		$this->enabled     					= $this->get_option( 'enabled' );
		$this->testmode    					= 'yes' === $this->get_option( 'testmode' );
		$this->logging     					= 'yes' === $this->get_option( 'logging' );
		$this->username    					= $this->testmode ? $this->get_option( 'test_username' ) : $this->get_option( 'username' );
		$this->password    					= $this->testmode ? $this->get_option( 'test_password' ) : $this->get_option( 'password' );
		$this->select_another_method_text	= $this->get_option( 'select_another_method_text' );
		if ( $this->testmode ) {
			$this->description .= ' TEST MODE ENABLED';
			$this->description = trim( $this->description );
		}
		// Hooks.
		add_action( 'admin_notices', array( $this, 'admin_notices' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'checkout_scripts' ) );
		add_action( 'woocommerce_api_wc_gateway_ecster', array( $this, 'osn_listener' ) );
		add_action( 'woocommerce_checkout_order_processed', array( $this, 'add_invoice_fee' ), 10, 2 );
	}

	/**
	 * Logging method.
	 *
	 * @param string $message
	 */
	public static function log( $message ) {
		$ecster_settings = get_option( 'woocommerce_ecster_settings' );
		if ( 'yes' === $ecster_settings['logging'] ) {
			if ( empty( self::$log ) ) {
				self::$log = new WC_Logger();
			}
			self::$log->add( 'ecster', $message );
		}
	}

	/**
	 * Listens for ping by Ecster, containing full order details.
	 */
	function osn_listener() {
		$post_body          = file_get_contents( 'php://input' );
		$decoded            = json_decode( $post_body );
		$internal_reference = $decoded->internalReference;
		$external_reference = $decoded->externalReference;
		$request            = new WC_Ecster_Request_Get_Order( $this->username, $this->password, $this->testmode );
		$response           = $request->response( $internal_reference );
		$response_body      = json_decode( $response['body'] );

		$this->log( 'OSN callback. Order ID:' . $_GET['order_id'] ); // Input var okay.
		$this->log( 'OSN callback. Response body:' . var_export( $response_body, true ) );

		if ( isset( $_GET['order_id'] ) ) { // Input var okay.
			$order_id = intval( $_GET['order_id'] ); // Input var okay.
			$order = wc_get_order( $order_id );

			if ( $order->get_user_id() > 0 ) {
				update_user_meta( $order->get_user_id(), 'billing_phone', $response_body->response->customer->cellular );
			}

			$order = wc_get_order( $order_id );
			$order->add_order_note( sprintf(
				__( 'Payment via Ecster Pay %s.', 'krokedil-ecster-pay-for-woocommerce' ),
				$response_body->response->paymentMethod->type
			) );
		} else {
			// Create local order here, since it wasn't created before, or notification URL update failed.
			$local_order = wc_create_order();
			foreach ( $response_body->response->order->rows as $order_row ) {
				if ( isset( $order_row->partNumber ) ) { // partNumber is only set for product order items.
					if ( isset( $product ) ) {
						unset( $product );
					}

					if ( wc_get_product( $order_row->partNumber ) ) { // If we got product ID.
						$product = wc_get_product( $order_row->partNumber );
					} else { // Get product ID based on SKU.
						global $wpdb;
						$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $order_row->partNumber ) );
						if ( $product_id ) {
							$product = wc_get_product( $product_id );
						}
					}

					if ( $product ) {
						$item_id = $local_order->add_product( $product, $order_row->quantity, array() );
						if ( ! $item_id ) {
							throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 525 ) );
						}
					}
				}
			}

			$local_order->update_status( 'on-hold' );
			$local_order->calculate_totals();
			$local_order->add_order_note( sprintf( __( 'There was a problem creating WooCommerce order, please check order %d in your Ecster dashboard.', 'krokedil-ecster-pay-for-woocommerce' ), $external_reference ) );
			$order_id = $local_order->id;
		}
		
		// Customer name
		if( $response_body->response->customer->name ) {
			$name_divider = $this->get_name_divider( $response_body->response->customer->name );
			$billing_name = explode( $name_divider, $response_body->response->customer->name );
		} else {
			$name_divider = $this->get_name_divider( $response_body->response->recipient->name );
			$billing_name = explode( $name_divider, $response_body->response->recipient->name );
		}
		
		if( 'NAME' == $response_body->response->order->idMethod  ) {
			// Manually entered name by customer in Ecsters iframe
			$billing_first_name = $billing_name[0];
			$billing_last_name = $billing_name[1];
		} else {
			// Name added via Bank ID or similar in Ecsters iframe
			$billing_first_name = $billing_name[1];
			$billing_last_name = $billing_name[0];
		}
		$billing_postcode 	= ( $response_body->response->customer->zip ?: $response_body->response->recipient->zip );
		$billing_address 	= ( $response_body->response->customer->address ?: $response_body->response->recipient->address );
		$billing_city 		= ( $response_body->response->customer->city ?: $response_body->response->recipient->city );
		$billing_country 	= $response_body->response->customer->countryCode ? $response_body->response->customer->countryCode : 'SE';

		update_post_meta( $order_id, '_billing_first_name', $billing_first_name );
		update_post_meta( $order_id, '_billing_last_name',  $billing_last_name );
		update_post_meta( $order_id, '_billing_email',      $response_body->response->customer->email );
		update_post_meta( $order_id, '_billing_postcode',   $billing_postcode );
		update_post_meta( $order_id, '_billing_address_1',  $billing_address );
		update_post_meta( $order_id, '_billing_city',  		$billing_city );
		update_post_meta( $order_id, '_billing_phone',      $response_body->response->customer->cellular );
		update_post_meta( $order_id, '_billing_country',    $billing_country );

		if ( $response_body->response->recipient ) {
			$name_divider = $this->get_name_divider( $response_body->response->recipient->name );
			$shipping_name = explode( $name_divider, $response_body->response->recipient->name );
			if( 'NAME' == $response_body->response->order->idMethod  ) {
				// Manually entered name by customer in Ecsters iframe
				$shipping_first_name = $shipping_name[0];
				$shipping_last_name = $shipping_name[1];
			} else {
				// Name added via Bank ID or similar in Ecsters iframe
				$shipping_first_name = $shipping_name[1];
				$shipping_last_name = $shipping_name[0];
			}
			update_post_meta( $order_id, '_shipping_first_name', $shipping_first_name );
			update_post_meta( $order_id, '_shipping_last_name',  $shipping_last_name );
			update_post_meta( $order_id, '_shipping_postcode',   $response_body->response->recipient->zip );
			update_post_meta( $order_id, '_shipping_address_1',  $response_body->response->recipient->address );
			update_post_meta( $order_id, '_shipping_country',    $response_body->response->recipient->countryCode );
		} else {
			update_post_meta( $order_id, '_shipping_first_name', $billing_first_name );
			update_post_meta( $order_id, '_shipping_last_name',  $billing_last_name );
			update_post_meta( $order_id, '_shipping_postcode',   $response_body->response->customer->zip );
			update_post_meta( $order_id, '_shipping_address_1',  $response_body->response->customer->address );
			update_post_meta( $order_id, '_shipping_country',    $billing_country );
		}

		update_post_meta( $order_id, '_wc_ecster_internal_reference', $internal_reference );
		update_post_meta( $order_id, '_wc_ecster_external_reference', $external_reference );
		update_post_meta( $order_id, '_wc_ecster_payment_method', $response_body->response->paymentMethod->type );

		header( 'HTTP/1.0 200 OK' );
	}

	/**
	 * Enqueue checkout page scripts
	 */
	function checkout_scripts() {
		if ( is_checkout() ) {
			if ( $this->testmode ) {
				wp_register_script( 'ecster_pay', 'https://labs.ecster.se/pay/integration/ecster-pay-labs.js', array(), false, false );
			} else {
				wp_register_script( 'ecster_pay', 'https://secure.ecster.se/pay/integration/ecster-pay.js', array(), false, true );
			}
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			$select_another_method_text = ( $this->select_another_method_text ?: __( 'Select another payment method', 'krokedil-ecster-pay-for-woocommerce' ) );
			wp_register_script(
				'ecster_checkout',
				WC_ECSTER_PLUGIN_URL . '/assets/js/frontend/checkout' . $suffix . '.js',
				array( 'ecster_pay', 'jquery' ),
				WC_ECSTER_VERSION,
				true
			);
			wp_localize_script( 'ecster_checkout', 'wc_ecster', array(
				'ajaxurl'                    => admin_url( 'admin-ajax.php' ),
				'terms'                      => wc_get_page_permalink( 'terms' ),
				'select_another_method_text' => $select_another_method_text,
				'wc_ecster_nonce'            => wp_create_nonce( 'wc_ecster_nonce' ),
			) );
			wp_enqueue_script( 'ecster_checkout' );
			wp_register_style(
				'ecster_checkout',
				WC_ECSTER_PLUGIN_URL . '/assets/css/frontend/checkout' . $suffix . '.css',
				array(),
				WC_ECSTER_VERSION
			);
			wp_enqueue_style( 'ecster_checkout' );
		}
	}

	/**
	 * Get_icon function.
	 *
	 * @access public
	 * @return string
	 */
	public function get_icon() {
		$ext   = version_compare( WC()->version, '2.6', '>=' ) ? '.svg' : '.png';
		$style = version_compare( WC()->version, '2.6', '>=' ) ? 'style="margin-left: 0.3em"' : '';
		$icon  = '';

		return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
	}
	
	/**
	 * get_name_divider function.
	 *
	 * The returned name from Ecster (in the OSN-listener) can be formatted in different ways. With and without comma character between first and last name.
	 *
	 * @access public
	 * @return string
	 */
	public function get_name_divider( $name ) {
		$has_comma = strpos( $name, ', ');
			if( false === $has_comma ) {
				return ' ';
			} else {
				return ', ';
			}
	}

	/**
	 * Check if SSL is enabled and notify the user
	 */
	public function admin_notices() {
		if ( 'no' === $this->enabled ) {
			return;
		}
		// Show notices here.
	}

	/**
	 * Check if this gateway is enabled
	 */
	public function is_available() {
		if ( 'yes' !== $this->enabled ) {
			return false;
		}

		/*
		$cart = WC()->cart->get_cart();
		foreach ( $cart as $cart_item_key => $values ) {
			$_product  = $values['data'];
			$tax_rates = WC_Tax::get_rates( $_product->get_tax_class() );
			foreach ( $tax_rates as $tax_rate ) {
				$tax_rate = intval( $tax_rate['rate'] );
				if ( ! in_array( $tax_rate, $this->allowed_tax_rates ) ) {
					return false;
				}
			}
		}
		*/

		return true;
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = include( 'settings-ecster.php' );
	}

	/**
	 * Payment form on checkout page
	 */
	public function payment_fields() {
		echo $this->description;
	}

	/**
	 * Process the payment
	 *
	 * @param int     $order_id Reference.
	 * @param boolean $retry    Retry processing or not.
	 *
	 * @return array
	 */
	public function process_payment( $order_id, $retry = false ) {
		$order = wc_get_order( $order_id );
		$order->payment_complete();

		WC()->cart->empty_cart();
		WC()->session->__unset( 'order_awaiting_payment' );
		WC()->session->__unset( 'wc_ecster_method' );

		return array(
			'result'   => 'success',
			'redirect' => $this->get_return_url( $order )
		);
	}

	/**
	 * Adds invoice fee to WooCommerce order
	 *
	 * @param int   $order_id WooCommerce order ID.
	 * @param array $posted   Array of posted data.
	 */
	public function add_invoice_fee( $order_id, $posted ) {
		if ( WC()->session->get( 'wc_ecster_invoice_fee' ) ) {
			$fees = WC()->session->get( 'wc_ecster_invoice_fee' );
			$order = wc_get_order( $order_id );

			foreach ( $fees as $fee ) {
				$ecster_fee            = new stdClass();
				$ecster_fee->id        = sanitize_title( __( 'Ecster Invoice Fee', 'krokedil-ecster-pay-for-woocommerce' ) );
				$ecster_fee->name      = __( 'Ecster Invoice Fee', 'krokedil-ecster-pay-for-woocommerce' );
				$ecster_fee->amount    = $fee / 100;
				$ecster_fee->taxable   = false;
				$ecster_fee->tax       = 0;
				$ecster_fee->tax_data  = array();
				$ecster_fee->tax_class = '';

				$fee_id                = $order->add_fee( $ecster_fee );
				$this->log( $fee_id );
				if ( ! $fee_id ) {
					$order->add_order_note( __( 'Unable to add Ecster invoice fee to the order.', 'krokedil-ecster-pay-for-woocommerce' ) );
				}
			}

			$order->calculate_totals( true );
			WC()->session->__unset( 'wc_ecster_invoice_fee' );
		}
	}

}