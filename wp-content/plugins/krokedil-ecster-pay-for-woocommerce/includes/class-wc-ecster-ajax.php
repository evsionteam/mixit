<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Ecster_Ajax class.
 */
class WC_Ecster_Ajax {

	/** @var string Ecster API username. */
	private $username;

	/** @var string Ecster API password. */
	private $password;

	/** @var boolean Ecster API testmode. */
	private $testmode;

	/**
	 * WC_Ecster_Ajax constructor.
	 */
	function __construct() {
		$ecster_settings = get_option( 'woocommerce_ecster_settings' );
		$this->testmode  = 'yes' === $ecster_settings['testmode'];
		$this->username  = $this->testmode ? $ecster_settings['test_username'] : $ecster_settings['username'];
		$this->password  = $this->testmode ? $ecster_settings['test_password'] : $ecster_settings['password'];

		add_action( 'wp_ajax_wc_ecster_create_cart', array( $this, 'ajax_create_cart' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_create_cart', array( $this, 'ajax_create_cart' ) );

		add_action( 'wp_ajax_wc_ecster_update_cart', array( $this, 'ajax_update_cart' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_update_cart', array( $this, 'ajax_update_cart' ) );

		add_action( 'wp_ajax_wc_ecster_fail_local_order', array( $this, 'ajax_fail_local_order' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_fail_local_order', array( $this, 'ajax_fail_local_order' ) );

		add_action( 'wp_ajax_wc_ecster_on_customer_authenticated', array( $this, 'ajax_on_customer_authenticated' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_on_customer_authenticated', array( $this, 'ajax_on_customer_authenticated' ) );

		add_action( 'wp_ajax_wc_ecster_on_changed_delivery_address', array( $this, 'ajax_on_changed_delivery_address' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_on_changed_delivery_address', array( $this, 'ajax_on_changed_delivery_address' ) );

		add_action( 'wp_ajax_wc_ecster_on_payment_success', array( $this, 'ajax_on_payment_success' ) );
		add_action( 'wp_ajax_nopriv_wc_ecster_on_payment_success', array( $this, 'ajax_on_payment_success' ) );
	}

	/**
	 * Creates Ecster cart.
	 */
	function ajax_create_cart() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) {
			WC_Gateway_Ecster::log( 'Nonce can not be verified - create_cart.' );
			exit( 'Nonce can not be verified.' );
		}

		$data     = array();
		$request  = new WC_Ecster_Request_Create_Cart( $this->username, $this->password, $this->testmode );
		$response = $request->response();
		$decoded  = json_decode( $response['body'] );

		if ( ! is_wp_error( $response ) && 200 == $response['response']['code'] ) {
			if ( is_string( $decoded->response->key ) ) {
				$data['wc_ecster_cart_key'] = $decoded->response->key;
				wp_send_json_success( $data );
			}
		} else {
			WC_Gateway_Ecster::log( 'Ecster create cart ' . $decoded->title . ': ' . $decoded->detail );
			$data['error_message'] = __( 'Ecster Pay create cart request failed', 'krokedil-ecster-pay-for-woocommerce' );
			wp_send_json_error( $data );
		}

		wp_die();
	}

	/**
	 * Updates Ecster cart.
	 */
	function ajax_update_cart() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) {
			WC_Gateway_Ecster::log( 'Nonce can not be verified - update_cart.' );
			exit( 'Nonce can not be verified.' );
		}
		$cart_key = $_POST['cart_key'];
		$data     = array();
		$request  = new WC_Ecster_Request_Update_Cart( $this->username, $this->password, $this->testmode );
		$response = $request->response( $cart_key );
		$decoded  = json_decode( $response['body'] );
		if ( ! is_wp_error( $response ) && 200 == $response['response']['code'] ) {
			if ( is_string( $decoded->response->key ) ) {
				$data['wc_ecster_cart_key'] = $decoded->response->key;
				wp_send_json_success( $data );
			}
		} else {
			WC_Gateway_Ecster::log( 'Ecster update cart ' . $decoded->title . ': ' . $decoded->detail );
			$data['error_message'] = __( 'Ecster Pay update cart request failed', 'krokedil-ecster-pay-for-woocommerce' );
			wp_send_json_error( $data );
		}
		wp_die();
	}

	/**
	 * Fails WooCommerce order.
	 */
	function ajax_fail_local_order() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) {
			WC_Gateway_Ecster::log( 'Nonce can not be verified - fail_local_order.' );
			exit( 'Nonce can not be verified.' );
		}
		$reason = $_POST['reason'];

		if ( WC()->session->get( 'order_awaiting_payment' ) > 0 ) {
			$local_order_id = WC()->session->get( 'order_awaiting_payment' );
			$local_order    = wc_get_order( $local_order_id );

			if ( 'failed' === $reason ) {
				$local_order->add_order_note( __( 'Ecster payment failed', 'krokedil-ecster-pay-for-woocommerce' ) );
			} elseif ( 'denied' === $reason ) {
				$local_order->add_order_note( __( 'Ecster payment denied', 'krokedil-ecster-pay-for-woocommerce' ) );
			}

			wp_send_json_success();
		} else {
			wp_send_json_error();
		}
		wp_die();
	}

	/**
	 * On customer authentication.
	 *
	 * Maybe create local order.
	 * Populate local order.
	 * Populate session customer data.
	 * Calculate cart totals.
	 * Update order cart hash.
	 */
	function ajax_on_customer_authenticated() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) { // Input var okay.
			WC_Gateway_Ecster::log( 'Nonce can not be verified - on_customer_authenticated.' );
			exit( 'Nonce can not be verified.' );
		}

		$customer_data = $_POST['customer_data']; // Input var okay.

		$local_order_id = $this->helper_maybe_create_local_order();
		update_post_meta( $local_order_id, '_wc_ecster_customer_authenticated', true );

		$this->helper_add_items_to_local_order( $local_order_id );
		$this->helper_add_customer_data_to_local_order( $local_order_id, $customer_data, array( 'billing', 'shipping' ) );
		$this->helper_add_customer_data_to_session( $customer_data, array( 'billing', 'shipping' ) );
		$this->helper_calculate_cart_totals();
		$this->helper_calculate_order_cart_hash( $local_order_id );

		wp_send_json_success( array( 'local_order_id' => $local_order_id ) );
		wp_die();
	}

	/**
	 * On changed delivery address.
	 *
	 * Maybe create local order.
	 * Populate local order.
	 * Populate session customer data.
	 * Calculate cart totals.
	 * Update order cart hash.
	 */
	function ajax_on_changed_delivery_address() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) { // Input var okay.
			WC_Gateway_Ecster::log( 'Nonce can not be verified - on_changed_delivery_address.' );
			exit( 'Nonce can not be verified.' );
		}

		$delivery_address = $_POST['delivery_address']; // Input var okay.

		$local_order_id = $this->helper_maybe_create_local_order();

		if ( get_post_meta( $local_order_id, '_wc_ecster_customer_authenticated', true ) ) {
			$addresses = array( 'shipping' );
		} else {
			$addresses = array( 'billing', 'shipping' );
		}

		$this->helper_add_items_to_local_order( $local_order_id );
		$this->helper_add_customer_data_to_local_order( $local_order_id, $delivery_address, $addresses );
		$this->helper_add_customer_data_to_session( $delivery_address, $addresses );
		$this->helper_calculate_cart_totals();
		$this->helper_calculate_order_cart_hash( $local_order_id );

		wp_send_json_success( array( 'local_order_id' => $local_order_id ) );
		wp_die();
	}

	/**
	 * On payment success.
	 *
	 * Maybe add invoice fee to order.
	 * Calculate cart totals.
	 * Update order cart hash.
	 */
	function ajax_on_payment_success() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'wc_ecster_nonce' ) ) { // Input var okay.
			WC_Gateway_Ecster::log( 'Nonce can not be verified - on_payment_success.' );
			exit( 'Nonce can not be verified.' );
		}

		$payment_data = $_POST['payment_data']; // Input var okay.

		$local_order_id = $this->helper_maybe_create_local_order();

		if ( $payment_data['fees'] ) {
			$this->helper_add_invoice_fees_to_session( $payment_data['fees'] );
		}

		$this->helper_calculate_order_cart_hash( $local_order_id );

		wp_send_json_success( array( 'local_order_id' => $local_order_id ) );
		wp_die();
	}


	/**
	 * Helpers.
	 */


	/**
	 * Creates WooCommerce order, if needed.
	 *
	 * @return int $local_order_id WooCommerce order ID.
	 */
	function helper_maybe_create_local_order() {
		if ( WC()->session->get( 'order_awaiting_payment' ) > 0 ) { // Create local order if there already isn't an order awaiting payment.
			$local_order_id = WC()->session->get( 'order_awaiting_payment' );
			$local_order    = wc_get_order( $local_order_id );
			$local_order->update_status( 'pending' ); // If the order was failed in the past, unfail it because customer was successfully authenticated again.
		} else {
			$local_order    = wc_create_order();
			$local_order_id = $local_order->id;
			WC()->session->set( 'order_awaiting_payment', $local_order_id );
			do_action( 'woocommerce_checkout_update_order_meta', $local_order_id, array() ); // Let plugins add their own meta data.
		}

		return $local_order_id;
	}

	/**
	 * Adds order items to ongoing order.
	 *
	 * @param  integer $local_order_id WooCommerce order ID.
	 * @throws Exception PHP Exception.
	 */
	function helper_add_items_to_local_order( $local_order_id ) {
		$local_order = wc_get_order( $local_order_id );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) { // Store the line items to the new/resumed order.
			$item_id = $local_order->add_product( $values['data'], $values['quantity'], array(
				'variation' => $values['variation'],
				'totals'    => array(
					'subtotal'     => $values['line_subtotal'],
					'subtotal_tax' => $values['line_subtotal_tax'],
					'total'        => $values['line_total'],
					'tax'          => $values['line_tax'],
					'tax_data'     => $values['line_tax_data'],
				),
			) );

			if ( ! $item_id ) {
				throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 525 ) );
			}

			do_action( 'woocommerce_add_order_item_meta', $item_id, $values, $cart_item_key ); // Allow plugins to add order item meta.
		}
	}

	/**
	 * Adds customer data to WooCommerce order.
	 *
	 * @param integer $local_order_id WooCommerce order ID.
	 * @param array   $customer_data  Customer data returned by Ecster.
	 * @param array   $addresses      Addresses to update (shipping and/or billing).
	 */
	function helper_add_customer_data_to_local_order( $local_order_id, $customer_data, $addresses ) {
		$name    = explode( ', ', $customer_data['name'] ); // Split the name into first and last.
		$country = isset( $customer_data['countryCode'] ) ? $customer_data['countryCode'] : 'SE';

		if ( in_array( 'billing', $addresses, true ) ) {
			update_post_meta( $local_order_id, '_billing_first_name', $name[1] );
			update_post_meta( $local_order_id, '_billing_last_name', $name[0] );
			update_post_meta( $local_order_id, '_billing_address_1', $customer_data['address'] );
			update_post_meta( $local_order_id, '_billing_city', $customer_data['city'] );
			update_post_meta( $local_order_id, '_billing_postcode', $customer_data['zip'] );
			update_post_meta( $local_order_id, '_billing_country', $country );
			if ( $customer_data['cellular'] ) {
				update_post_meta( $local_order_id, '_billing_phone', $customer_data['cellular'] );
			}
			if ( $customer_data['email'] ) {
				update_post_meta( $local_order_id, '_billing_email', $customer_data['email'] );
			}
		}

		if ( in_array( 'shipping', $addresses, true ) ) {
			update_post_meta( $local_order_id, '_shipping_first_name', $name[1] );
			update_post_meta( $local_order_id, '_shipping_last_name', $name[0] );
			update_post_meta( $local_order_id, '_shipping_address_1', $customer_data['address'] );
			update_post_meta( $local_order_id, '_shipping_city', $customer_data['city'] );
			update_post_meta( $local_order_id, '_shipping_postcode', $customer_data['zip'] );
			update_post_meta( $local_order_id, '_shipping_country', $country );
		}
	}

	/**
	 * Adds customer data to WooCommerce order.
	 *
	 * @param array $customer_data Customer data returned by Ecster.
	 * @param array $addresses     Addresses to update (shipping and/or billing).
	 */
	function helper_add_customer_data_to_session( $customer_data, $addresses ) {
		$country = isset( $customer_data['countryCode'] ) ? $customer_data['countryCode'] : 'SE';

		if ( in_array( 'billing', $addresses, true ) ) {
			WC()->customer->set_country( $country );
			WC()->customer->set_postcode( $customer_data['zip'] );
			WC()->customer->set_city( $customer_data['city'] );
			WC()->customer->set_address( $customer_data['address'] );
		}

		if ( in_array( 'shipping', $addresses, true ) ) {
			WC()->customer->set_shipping_country( $country );
			WC()->customer->set_shipping_postcode( $customer_data['zip'] );
			WC()->customer->set_shipping_city( $customer_data['city'] );
			WC()->customer->set_shipping_address( $customer_data['address'] );
		}
	}

	/**
	 * Adds order items to ongoing order.
	 *
	 * @param integer $local_order_id WooCommerce order ID.
	 */
	function helper_calculate_order_cart_hash( $local_order_id ) {
		update_post_meta( $local_order_id, '_cart_hash', md5( wp_json_encode( wc_clean( WC()->cart->get_cart_for_session() ) ) . WC()->cart->total ) );
	}

	/**
	 * Calculates cart totals.
	 */
	function helper_calculate_cart_totals() {
		WC()->cart->calculate_fees();
		WC()->cart->calculate_shipping();
		WC()->cart->calculate_totals();
	}

	/**
	 * Adds invoice fee to WooCommerce order
	 *
	 * @param array $fees Array of fees returned by Ecster.
	 * @TODO: Fix this
	 */
	function helper_add_invoice_fees_to_session( $fees ) {
		WC()->session->set( 'wc_ecster_invoice_fee', $fees );
	}

}
$wc_ecster_ajax = new WC_Ecster_Ajax;
