<?php
	get_header();
	global $mixit_option;
	get_redux_page_banner();
	the_post();
	$post_id = get_the_ID();
?>
<!-- Designer Listing Template -->
<div class="designer-template-wrapper white-bg">
	<div class="title-designer text-center">
		<h1><?php echo get_post_meta( $post_id, 'title_below_banner', true ); ?></h1>
		<p><?php  echo get_post_meta( $post_id, 'description_below_banner', true ); ?></p>
	</div>

	<div class="container">
		<div class="row">
			<?php the_content(); ?>
		</div>
	</div>

	<!-- Shop Brand Wrapper -->
	<?php /* ?>
	<div class="shop-by-brand-wrapper text-center">
		<h3 class="title"><?php echo $mixit_option['shop_by_brand_title'] ?></h3>
		<div class="description"><?php echo $mixit_option['shop_by_brand_description']; ?></div>
		<div class="mixit-brand_carousel">
			<div id="mixit-homepage-caroufredsel" >
				<?php 
					$terms = get_terms( array(
					    'taxonomy' => 'brand',
					    'hide_empty' => false,
					));
				?>	
				<?php foreach( $terms as $t ): ?>
					<?php
						$t_id = $t->term_id;
						$term_meta = get_option( "brand_$t_id" );
						$brandimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : ''; 
					?>
					<div class='brand-item' style="background-image: url('<?php echo $brandimage; ?>')" ></div>
				<?php endforeach; ?>
			</div>
		</div>

		<div id="mixit-homepage-caroufredsel-pagination" class="shop-brand-pagination"></div>
	</div>
	<?php */ ?>
</div>

<!-- Above the footer -->
<div class="customer-block">
	<?php if ( is_active_sidebar( 'above-footer' ) ) : ?>
			<?php dynamic_sidebar( 'above-footer' ); ?>
	<?php endif; ?>
</div>

<!-- Above the footer -->
<?php get_footer(); ?>


