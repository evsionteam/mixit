<?php
show_admin_bar( false );
add_action( 'after_setup_theme', 'mixit_setup' );
if ( ! function_exists( 'mixit_setup' ) ) :

	function mixit_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Mixit, use a find and replace
		 * to change 'mixit' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mixit', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		add_theme_support( 'woocommerce' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'mixit' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mixit_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_image_size( 'featured-designer', 395, 405, true );
		add_image_size( 'designer', 395, 200, true );
	}
endif;


add_action( 'after_setup_theme', 'mixit_content_width', 0 );
function mixit_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mixit_content_width', 640 );
}

add_action( 'widgets_init', 'mixit_widgets_init' );
function mixit_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mixit' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mixit' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'mixit' ),
		'id'            => 'blog-sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mixit' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	for( $i = 1; $i<5; $i++ ){

		$name = 'Footer '.$i;
		$id = 'footer-'.$i;

		if( $i ==4 ){
			$before_widget_class = 'col-md-3 col-sm-6 column contact-block';
		}else{
			$before_widget_class = 'col-md-2 col-sm-6 column';
		}

		register_sidebar( array(
			'name'          => esc_html__( $name, 'mixit' ),
			'id'            => $id,
			'description'   => esc_html__( 'Add widgets here. It will appear in footer.', 'mixit' ),
			'before_widget' => '<div class="'.$before_widget_class.'">',
			'after_widget'  => '</div>',
			'before_title'  => '<strong class="title">',
			'after_title'   => '</strong>',
		));
	}

	register_sidebar( array(
		'name'          => esc_html__( 'Shop Sidebar', 'mixit' ),
		'id'            => 'shop-sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mixit' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	

	register_sidebar( array(
		'name'          => esc_html__( 'Above footer', 'mixit' ),
		'id'	        => 'above-footer',
		'description'   => esc_html__( 'Add widgets here', 'mixit' ),
		'before_widget' =>	'<div class="col-sm-4 customer-column">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

add_action( 'wp_enqueue_scripts', 'mixit_scripts' );
function mixit_scripts() {

	$assets_url = get_stylesheet_directory_uri().'/assets/build';

	$suffix = '.min';

	if( defined('SCRIPT_DEBUG') && TRUE === SCRIPT_DEBUG  ){
		$suffix = '';
	}

	wp_enqueue_style( 'mixit-style', get_stylesheet_uri(), array( 'wp-jquery-ui-dialog' ) );

	// enqueue wp share style
	wp_enqueue_style( 'wpallshare-default' );
	wp_enqueue_style( 'wpallshare-theme4' );

	wp_add_inline_style( 'mixit-style','body{
		opacity: 0
	}');

	wp_enqueue_script( 'mixit-script',
		$assets_url . '/js/script' . $suffix . '.js',
		array( 'jquery','jquery-ui-dialog' ), false, true );


	$main_css = $assets_url . '/css/main'.$suffix.'.css';
	$script = "
	   var head  = document.getElementsByTagName('head')[0];
	   var link  = document.createElement('link');
	   link.rel  = 'stylesheet';
	   link.type = 'text/css';
	   link.href = '".$main_css."';
	   link.media = 'all';
	   head.appendChild(link);
	";


    wp_add_inline_script( 'mixit-script',$script );

    $webfont = "WebFontConfig = {
			google: { families: [ 'Open+Sans:400,400italic,600,600italic,300,300italic,700,700italic,800:latin' ] }
		 };
		 (function() {
		   var wf = document.createElement('script');
		   wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		   wf.type = 'text/javascript';
		   wf.async = 'true';
		   var s = document.getElementsByTagName('script')[0];
		   s.parentNode.insertBefore(wf, s);
		 })(); ";

	wp_add_inline_script('mixit-script', $webfont );

	wp_localize_script( 'mixit-script', 'self', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

	//johan's css
	wp_enqueue_style( 'johan', get_stylesheet_directory_uri() . '/johan.css' );
}

add_action( 'admin_enqueue_scripts', 'mixit_admin_script' );
function mixit_admin_script(){
	 wp_enqueue_media();
}

//add_filter( 'wp_nav_menu_items', 'mixit_custom_menu_item', 10, 2 );
function mixit_custom_menu_item ( $items, $args ) {

	global $woocommerce;

	$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

	$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
	if ( $myaccount_page_id ) {
	  $myaccount_page_url = get_permalink( $myaccount_page_id );
	}

	$cart_url = $woocommerce->cart->get_cart_url();

	$checkout_url = $woocommerce->cart->get_checkout_url();

	ob_start();
	woocommerce_mini_cart();

	$woocommerce_mini_cart = '<ul class="sub-menu"><li id="mixit-cart-detail">'.ob_get_clean().'</li></ul>';
	ob_flush();

    if ( $args->theme_location == 'primary') {
        $items  .=  '<li class="mixit-custom-menu-item menu-item-has-children">
						<a href="'.$myaccount_page_url.'">
							<i class="mm-icon fa fa-user"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="'.$myaccount_page_url.'">
									<i class="mm-icon fa fa-user"></i>
									'.__( 'My Account', 'mixit' ).'
								</a>
							</li>

							<li>
								<a href="'.$cart_url.'">
									<i class="mm-icon fa fa-shopping-cart"></i>
									'.__( 'Cart', 'mixit' ).'
								</a>
							</li>
							<li>
								<a href="'.$checkout_url.'">
									<i class="mm-icon fa fa-edit"></i>
									'.__( 'Checkout', 'mixit' ).'
								</a>
							</li>
						</ul>
					</li>';

		$items .=  '<li class="mixit-custom-menu-item mixit-product-search">
						<a href="">
							<i class="fa fa-search"></i>
						</a>
					</li>';


		$items  .=  '<li class="mixit-custom-menu-item mixit-cart-detail menu-item-has-children">
						<span class="cart-item-count" id="cart-item-count">'.$woocommerce->cart->cart_contents_count.'</span>
						<a href=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>'.$woocommerce_mini_cart.'
					</li>';
    }
    return $items;
}

//add_action( 'brand_add_form_fields', 'mixit_add_form_field_term_meta_image' );
function mixit_add_form_field_term_meta_image() { ?>
    <?php wp_nonce_field( basename( __FILE__ ), 'term_meta_image_nonce' ); ?>
    <div class="form-field term-meta-text-wrap">
        <label for="term-meta-image"><?php _e( 'Brand Image', 'mixit' ); ?></label>
    </div>
<?php }


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

require get_template_directory() . '/admin/redux/config.php';

require get_template_directory() . '/inc/maid.php';
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/woo-templates.php';
require get_template_directory() . '/inc/woo-functions.php';
require get_template_directory() . '/inc/post-types.php';

require get_template_directory() . '/inc/page-hook.php';

require get_template_directory() . '/inc/product-hook.php';

require get_template_directory() . '/inc/woo-brand.php';



function mixit_comment( $comment, $args, $depth ){
	?>

	<li id="li-comment-<?php comment_ID() ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> >
		<div id="comment-<?php comment_ID() ?>" class="comment_container">
			<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			<div class="comment-text">
				<p class="meta">
					<strong> <?php echo  get_comment_author_link(); ?> </strong>

					<time>
						<?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?>
					</time>

					<?php if ( $comment->comment_approved == '0' ) : ?>
						<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
					<?php endif; ?>
				</p>

				<div class="description">
					<p><?php comment_text(); ?></p>
				</div>

				<div class="reply">
					<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>

					<?php
						$param = array( 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] );
						comment_reply_link( array_merge( $args, $param ) );
					?>
				</div>
			</div> <!-- comment-text -->
		</div>
	</li>
<?php
}


add_action( 'init', 'mixit_testimonial_init' );
function mixit_testimonial_init(){
	$labels = array(
		'name'               => _x( 'Testimonial', 'Testimonial', 'mixit' ),
		'singular_name'      => _x( 'Testimonial', 'Testimonial', 'mixit' ),
		'menu_name'          => _x( 'Testimonial', 'Testimonials', 'mixit' ),
		'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'mixit' ),
		'add_new'            => _x( 'Add New', 'Testimonial', 'mixit' ),
		'add_new_item'       => __( 'Add New Testimonial', 'mixit' ),
		'new_item'           => __( 'New Testimonial', 'mixit' ),
		'edit_item'          => __( 'Edit Testimonial', 'mixit' ),
		'view_item'          => __( 'View Testimonial', 'mixit' ),
		'all_items'          => __( 'All Testimonial', 'mixit' ),
		'search_items'       => __( 'Search Testimonial', 'mixit' ),
		'parent_item_colon'  => __( 'Parent Testimonial:', 'mixit' ),
		'not_found'          => __( 'No Testimonials found.', 'mixit' ),
		'not_found_in_trash' => __( 'No Testimonials found in Trash.', 'mixit' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'User Testimonials.', 'mixit' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);

	register_post_type( 'testimonial', $args );
}

add_filter( 'comment_form_defaults', 'mixit_comment_form_defaults' );
function mixit_comment_form_defaults( $default ){
	$commenter = wp_get_current_commenter();
	$fields   =  array(
		'author' => '<p class="comment-form-author">' .
		            '<input id="author" placeholder="'.__( 'Name', 'mixit' ).'" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"' . $aria_req . $html_req . ' /></p>',
		'email'  => '<p class="comment-form-email"><input placeholder="'.__( 'Email', 'mixit' ).'" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>',
		'url'    => '<p class="comment-form-url"><input placeholder="'.__( 'Website','mixit' ).'" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>',
	);

	$default['fields'] = $fields;
	$default[ 'title_reply_before' ] = '<h2  class="comment-reply-title">';
	$default[ 'title_reply_after' ] = '</h2>';

	return $default;
}

add_action( 'edit_form_before_permalink', 'mixit_box_after_title' );
function mixit_box_after_title( $post ) {
    if( $post->post_type == 'page' ):
        $page_subtitle = get_post_meta( $post->ID, 'page_subtitle', true );
        ?>
        <input type="text" name="page_subtitle" class="regular-text" value="<?php echo $page_subtitle; ?>"
        	style="width:100%;"
        	placeholder="<?php _e('Subtitle','mixit'); ?>" />
    <?php
    endif;
}


add_action( 'save_post', 'save_subtitle_of_page',10,2 );
function save_subtitle_of_page($post_id, $post){
    $page_subtitle = isset($_POST['page_subtitle'])?strip_tags($_POST['page_subtitle']):'';
    update_post_meta( $post_id, 'page_subtitle', $page_subtitle );
}

function get_redux_page_banner(){
	global $post, $mixit_option, $wp_query;

	?>
	<div class="page-banner-image">
		<div class="banner-centered-title-header">
		<h1 class="banner-centered-title" >
			<?php if( is_home() ){  
				single_post_title();
			  }elseif (is_archive()) {
			  	echo $wp_query->get_queried_object()->name;
			  } else{
				the_title();
				}
			?>
		</h1>
		</div>

		<?php
			$image_id = get_post_meta( $post->ID, '_image_id', true );
		 	$image_src = wp_get_attachment_url( $image_id );
		   	$image = wp_get_attachment_url( $image_id );
		 if( $image) :
		 	?>
		 	<img src="<?php echo $image_src; ?>" width="100%" class="img-responsive" alt="Cover Image" />
		<?php  else : ?>
				<img src="<?php echo $mixit_option[ 'site_banner' ]['url']; ?>" width="100%" class="img-responsive" alt="Banner Image" />
		<?php endif;?>


		<?php /*if( !empty( $mixit_option[ 'site_banner']['url'] ) ):  ?>
			<img src="<?php echo $mixit_option[ 'site_banner' ]['url']; ?>"
				width="100%"
				class="img-responsive" alt="Banner Image" />
		<?php else: ?>
			<img src="//placeholdit.imgix.net/~text?txtsize=25&txt=Banner&w=1200&h=500&txttrack=0"
				class="img-responsive" width="100%" alt="Header Image" />
		<?php endif; */?>

		<div class="breadcrumb-holder"><?php woocommerce_breadcrumb(); ?></div>
	</div><!-- /page-banner-image -->
	<?php
}

function mixit_custom_excerpt_length( $length ) {
	if( !is_home() ) return $length;
    return 45;
}
add_filter( 'excerpt_length', 'mixit_custom_excerpt_length', 999 );

function modify_unavailable_text( $data ){

	$data['i18n_unavailable_text'] = 'Tyvärr så är denna storlek/kombination av produkt slut. Vänligen försök med någon annan. Det går även bra att ringa till oss och fråga, så kanske vi kan beställa hem rätt storlek till dig.';

	return $data;
}
add_filter( 'wc_add_to_cart_variation_params', 'modify_unavailable_text' );