<div>
	<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>" >
	    <span>
	    	<label class="screen-reader-text" for="s"><?php  _e( 'Search for:' ); ?></label>
	    	<input placeholder="<?php _e( 'Search','mixit' ); ?>" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
	    </span>
	    <span>
	    	<button id="searchsubmit" type="submit"><i class="fa fa-search"></i></button>
	    </span>
    </form>
</div>