<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : 
				the_post(); 
				$post_id = get_the_title();
		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php get_template_part( 'template-parts/content', 'banner' ); ?>

			<div>
				<div class="clearfix post-page mixit-my-account-page">
					<div class="col-sm-4 col-md-3">
						<div class="my-account-sidebar-wrapper">
							<?php get_sidebar(); ?>
						</div>
					</div>
					<div class="col-sm-8 col-md-9">
						<div class="my-account">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div><!-- .container -->
		</article>
		
	<?php endwhile;  ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
