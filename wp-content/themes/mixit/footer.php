
<?php 
	wp_footer(); 
	global $mixit_option;
	$footer_logo = $mixit_option['footer_logo']['url'];
?>
<div id="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12 column">
					<?php if( !empty( $footer_logo ) ): ?>
						<a class="footer-logo" href="/">
							<img alt="Mixit - Kläder för han och henne" 
							src="<?php echo $footer_logo; ?>" width="180" height="63" />
						</a>
					<?php endif; ?>
				</div>

				<?php 
					for( $i=1; $i<5; $i++ ){

						$sidebar = 'footer-'.$i;
						
						if ( is_active_sidebar( $sidebar ) ){
							dynamic_sidebar( $sidebar );
							if( $i == 2 ){
								echo "<div class='clearfix visible-sm'></div>";
							}
						}
					}
				?>
			</div>
		</div>
		<div class="footer-bottom"><?php echo $mixit_option[ 'footer_text' ]; ?></div>
		
	</div>
	</div>

</div>
</body>
</html>