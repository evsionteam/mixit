<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Mixit
 */
	$post_id = get_the_id();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'mixit-post' ); ?>>
	<div class="row">
		<?php if( has_post_thumbnail() ):  ?>

			<div class="col-sm-4"><div class="post-image">
				<?php echo get_the_post_thumbnail( null, $size, array( 'width' => '100%' ) ); ?>
				<div class="icon">
					<i class="fa fa-picture-o"></i>
				</div>
			</div></div>

		<?php endif; ?>

		<div class="<?php echo has_post_thumbnail() ? 'col-sm-8' : ''; ?> main-content-holder">
			<header class="entry-header">
				<?php
				
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				

				if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php 
							mixit_posted_on(); 
							echo ' | ';
							$categories_list = get_the_category_list( esc_html__( ', ', 'mixit' ) );
							if ( $categories_list && mixit_categorized_blog() ) {
								printf( '<span class="cat-links">' . esc_html__( 'Kategori %1$s', 'mixit' ) . '</span>', $categories_list ); // WPCS: XSS OK.
							}
						?>
					</div><!-- .entry-meta -->
				<?php
				endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_excerpt(); ?>
				
			</div><!-- .entry-content -->
			<div class="read-blog">
				<a href="<?php the_permalink(); ?>"><?php _e( 'LAS MER', 'mixit' ) ?>  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>

			
		</div>
	</div>
</article><!-- #post-## -->
