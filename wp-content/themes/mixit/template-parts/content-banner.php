<?php if( has_post_thumbnail() ): ?>
	<style>
		.page-banner-image{ background-image: url("<?php the_post_thumbnail_url(); ?>"); }
	</style>
<?php endif; ?>
<div class="page-banner-image">  
	<div class="banner-centered-title-header">
		<h2 class="banner-centered-title" ><?php the_title();  ?></h2>
	</div>
	<?php if( has_post_thumbnail() ):  ?>
		<?php echo get_the_post_thumbnail( null, $size, array( 'width' => '100%', 'class' => '' ) ); ?>
	<?php else: ?>
		<img src="//placeholdit.imgix.net/~text?txtsize=25&txt=Banner&w=1200&h=500&txttrack=0" class="img-responsive hidden-xs" width="100%" alt="Header Image" />
	<?php endif; ?>
	<div class="breadcrumb-holder"><?php woocommerce_breadcrumb(); ?></div>
</div>
