<div id="popup-after-add-to-cart" class="lastudio-global-message" style="display: none;">
    <div class="wrapper-content">
        <span class="close-message"><i class="fa fa-times"></i></span>
        <div class="inner-content-message">
	        <div class="popup-product">
		        <img width="300" height="300" src="" 
		        	class="attachment-shop_catalog size-shop_catalog wp-post-image" 
		        	alt="T_7_front" />
		        
		        <h5 id="added-to-cart-name"></h5>

		        <div class="popup-message highlight-font-family font-style-italic">
		        	<?php _e( 'Was successfully added to your cart' ); ?>
		        </div>
	        </div>

        <a class="button popup-button-continue" href="<?php echo $shop_page_url; ?>">
        	<?php _e( 'Continue Shopping', 'mixit' ); ?>
        </a>
        <a class="button primary view-popup-addcart" href="<?php echo $cart_page_url; ?>"><?php _e( 'Visa varukorg', 'mixit' ); ?></a>
        </div>
        <div class="close-message-text">
        	<span>
        		<?php _e( 'The message will be closed after','mixit' ); ?> 
        		<i>10</i> s
        	</span>
        </div>
    </div>
</div>

<div id="dialog-product" style="display: none;">
    <div id="content" class="product-single-wrapper">
        <div class="product"></div>
    </div>
</div>