<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Mixit
 */
	$post_id = get_the_id();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'mixit-post' ); ?>>
	
	<?php if( has_post_thumbnail() ):  ?>

		<div class="post-image">
			<?php echo get_the_post_thumbnail( null, $size, array( 'width' => '100%' ) ); ?>
			<div class="icon">
				<i class="fa fa-picture-o"></i>
			</div>
		</div>

	<?php endif; ?>

	<div class="main-content-holder">
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			elseif( is_page() ):
				$page_subtitle = get_post_meta( $post->ID, 'page_subtitle', true );
				echo '<h2 class="entry-title">'.$page_subtitle.'</h2>';
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta">
					<?php mixit_posted_on(); ?>
					<span class="comment-counter">
						<i class="fa fa-comments-o" aria-hidden="true"></i>
						<span><?php echo  wp_count_comments( $post_id )->total_comments; ?></span>
					</span>
				</div><!-- .entry-meta -->
			<?php
			endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
				the_content();
			?>
			
		</div><!-- .entry-content -->

		<div class="content-links">
				<?php mixit_entry_footer(); ?>
		</div>
		<div class="content-page-link"> 
			<?php 
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mixit' ),
					'after'  => '</div>',
				) ); 
			?>
		</div>
	</div>
</article><!-- #post-## -->
