<?php

$args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'orderby' => 'menu_order title',
    'order' => 'ASC',
    'posts_per_page' => 20
);
/**/
if(!empty($sk_cat_slug)){
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => $sk_cat_slug,
        ),
    );
}

$sk_products = new WP_Query($args);
global $unique_products;
if ( $sk_products->have_posts() ) : 
    ?>

    <?php // woocommerce_product_loop_start(); ?>
   
    <?php  woocommerce_product_subcategories(); ?>

    <?php while ( $sk_products->have_posts() ) : $sk_products->the_post(); ?>

        <?php $id = get_the_ID(); if( !in_array( $id, $unique_products, true )): $unique_products[] = $id; ?>
            <?php wc_get_template_part( 'content', 'product' ); ?>
        <?php endif; ?>

    <?php endwhile; // end of the loop. ?>
   
    <?php //woocommerce_product_loop_end(); ?>

    <?php
    global $wp_query;
    $wp_query = $sk_products;
    ?>

<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

    <?php wc_get_template( 'loop/no-products-found.php' ); ?>

<?php endif; ?>
