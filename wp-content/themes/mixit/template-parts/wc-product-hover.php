<?php
	global $product;
?>
<div class="product-action-hover">
	<div class="action-hover-inner">
		<div class="button-wrapper">
			<a data-product-id="<?php  echo $product->id; ?>" class="quickview button lastudio-quickview-button" 
				href="<?php echo get_the_permalink(); ?>" title="<?php _e( 'Quick View', 'mixit' ); ?>">
				<span>
					<span class="search-wrap">
						<span class="icon"><i class="fa fa-search"></i></span>
						<span class="text hidden"><?php _e( 'Quick View', 'mixit' ); ?></span>
					</span>
					<span class="loader-search"></span>
				</span>
			</a>
		</div>
		
		<div class="button-wrapper">
			<?php woocommerce_template_loop_add_to_cart(); ?>
		</div>					
	</div>
</div>