<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

		<link rel="profile" href="http://gmpg.org/xfn/11">

		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php wp_head(); ?>

	</head>

	<?php 

		global $mixit_option;

		$header_logo = $mixit_option['header_logo']['url'];

	?>

	<body <?php body_class(); ?>>

	<?php 

		if( is_shop() || is_front_page() || is_product() ):

			get_template_part( 'template-parts/product', 'popup' );

		endif;

	?>

		<div id="main-page">

		<!-- 

			//for mmenu xs

			<div id="mixit-mmenu" >

				<?php //wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>

			</div>

		-->

			<div style="display:none;" id="search-wrapper">

				<form role="search" method="get" class="woocommerce-product-search mixit-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">

					<input type="search" id="woocommerce-product-search-field" class="search-field" 

						placeholder="<?php echo esc_attr_x( 'Search Products..', 'placeholder', 'woocommerce' ); ?>" 

						value="<?php echo get_search_query(); ?>" 

						name="s" 

						title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />

						<input type="hidden" name="post_type" value="product" />

					<button type="submit">

						<i class="fa fa-search"></i>

					</button>

				</form>

			</div>

			<header id="header" class="site-header">

				<div class="container">

					<div class="logo col-sm-2 col-xs-4">

						<?php if( !empty( $header_logo ) ): ?>

							<a href="<?php echo get_home_url(); ?>">

								<img class="img-responsive" alt="Mixit - KlĆ¤der fĆ¶r han och henne" src="<?php echo $header_logo; ?>" width="180" height="63" />

							</a>

						<?php endif; ?>

					</div><!-- logo -->

					<div class="right-panel col-md-10">

					<?php

							global $woocommerce;



							$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );



							$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );

							if ( $myaccount_page_id ) {

							  $myaccount_page_url = get_permalink( $myaccount_page_id );

							}



							$cart_url = $woocommerce->cart->get_cart_url();



							$checkout_url = $woocommerce->cart->get_checkout_url();

						?>

						<div class="mixit-user-info-wrapper">

							<ul class="mixit-user-info">

								<li class="mixit-account">

									<a href="<?php echo $myaccount_page_url; ?>">

										<i class="mm-icon fa fa-user"></i>

									</a>

									<ul>

										<li>

											<a href="<?php echo $myaccount_page_url; ?>">

												<i class="mm-icon fa fa-user"></i>

												<?php  _e( 'My Account', 'mixit' ); ?>

											</a>

										</li>

										

										<li>

											<a href="<?php echo $cart_url; ?>">

												<i class="mm-icon fa fa-shopping-cart"></i>

												<?php _e( 'Cart', 'mixit' ); ?>

											</a>

										</li>

										<li>

											<a href="<?php echo $checkout_url; ?>">

												<i class="mm-icon fa fa-edit"></i>

												<?php _e( 'Checkout', 'mixit' ); ?>

											</a>

										</li>



									</ul>

								</li>

								<li class="mixit-search">

									<i class="fa fa-search"></i>

								</li>

								<li class="mini-cart-wrapper">

									<div class="mini-cart">

										<div class="cart-info">

											<span class="cart-item-count" id="cart-item-count">

												<?php echo $woocommerce->cart->cart_contents_count; ?>

											</span>

											<span class="mini-cart-icon">

												<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>

											</span>

										</div>

										<div id="mixit-cart-detail">

											<div class="widget_shopping_cart_content">

												<?php woocommerce_mini_cart(); ?>

											</div>

										</div>

									</div>

								</li>

								<!--

									//Hamburger button

								 <li class="visible-sm visible-xs" ><a href="#mixit-mmenu" class="mixit-menu-toggler"> <i class="fa fa-bars"></i> </a></li> -->

							</ul>

						</div>

						<div class="main-header-navigation">

							<?php 

								// $nav_args = array(

								// 	'menu_class' => 'list-inline mixit-menu-wrapper',

								// 	'theme_location' => 'primary'

								// ); 

								//wp_nav_menu( $nav_args ); 

								if( function_exists( 'ubermenu' ) ):

									ubermenu( 'main' , array( 'theme_location' => 'primary' ) );

								endif;

							?>



						</div><!-- main-navigation -->

						

					</div><!-- right -->

				</div><!-- /container -->

			</header>