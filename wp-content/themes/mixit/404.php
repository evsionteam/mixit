<?php get_header(); global $mixit_option; ?>

<?php if(isset( $mixit_option[ 'site_banner' ][ 'url'] ) && !empty(  $mixit_option[ 'site_banner' ][ 'url'] ) ): ?>
	<style>
		.page-banner-image{
			background-image: url("<?php  echo $mixit_option[ 'site_banner' ][ 'url']; ?>");
		}
	</style>
<?php endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="page-banner-image">  
				<div class="banner-centered-title-header">
					<h2 class="banner-centered-title" ><?php _e( '404 - Page Not Found', 'mixit' );  ?></h2>
				</div>
				<?php if( !empty( $mixit_option[ 'site_banner' ][ 'url'] ) ):  ?>
					<img src="<?php echo $mixit_option[ 'site_banner' ][ 'url']; ?>" 
						class="img-responsive hidden-xs" alt="Site Banner" />
				<?php endif; ?>
				<div class="breadcrumb-holder">
				<?php woocommerce_breadcrumb(); ?>
				</div>
			</div><!-- /page-banner-image -->

			<div class="text-center content-404">
				<h2><?php _e( '404', 'mixit' ); ?></h2>
				<p class="subtitle-404"><?php _e( 'OOPS! PAGE NOT FOUND' , 'mixit' ); ?></p>
				<p class="description-404">
					<?php _e( 'SORRY, BUT THE PAGE YOU ARE LOOKING FOR IT NOT FOUND. PLEASE, MAKE SURE YOU HAVE TYPED THE CURRENT URL.', 'mixit' ); ?>
				</p>
				<a href="<?php echo get_home_url(); ?>" class="btn-404">
					<?php _e( 'GOTO HOMEPAGE', 'mixit' ); ?>
				</a>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
