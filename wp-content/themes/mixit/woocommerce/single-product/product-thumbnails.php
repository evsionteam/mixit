<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>
	<div class="thumbnails <?php echo count( $attachment_ids ) < 5 ? 'no-caroufredsel' : ''; ?>">
		<div id="product-thumbnail-caroufredsel" class="clearfix">
			<?php

				//Thumbnail for post image
				$thumb_id = get_post_thumbnail_id();
				array_unshift( $attachment_ids, $thumb_id );

				//Thumbnails for product gallery
				foreach ( $attachment_ids as $key => $attachment_id ) {

					$classes[] = '';
					$image_class = implode( ' ', $classes );
					$props       = wc_get_product_attachment_props( $attachment_id, $post );

					if ( ! $props['url'] ) {
						continue;
					}

					$thumb_url = wp_get_attachment_image_src($attachment_id,'full', true);
					$large = $thumb_url[0];

					$thumb_url = wp_get_attachment_image_src($attachment_id,'shop_single', true);
					$main_img = $thumb_url[0];
					
					$active_class = '';
					if( 0 == $key ){
						$active_class = 'active-img';
					}

					echo apply_filters(
						'woocommerce_single_product_image_thumbnail_html',
						sprintf(
							'<div data-main-img="%s" data-large-img="%s" class="product-img-thumb '. $active_class. '" href="%s" class="%s" title="%s">%s</div>',
							$main_img,
							$large,
							esc_url( $props['url'] ),
							esc_attr( $image_class ),
							esc_attr( $props['caption'] ),
							wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $props )
						),
						$attachment_id,
						$post->ID,
						esc_attr( $image_class )
					);


				}
			?>
		</div>
		<div class="product-thumbnail-paginattion">
			<span id="product-thumb-next"><i class="fa fa-angle-left"></i></span>
			<span id="product-thumb-prev"><i class="fa fa-angle-right"></i></span>
		</div>
	</div>
	<?php
}
