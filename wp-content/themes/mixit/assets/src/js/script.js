function scrollToTop ( param ){

	this.markup   = null,
	this.selector = null;
	this.fixed    = true;
	this.visible  = false;

	this.init = function(){

		if( this.valid() ){

			if( typeof param != 'undefined' && typeof param.fixed != 'undefined' ){
				this.fixed = param.fixed;
			}

			this.selector = ( param && param.selector ) ? param.selector : '#scroll-to-top';

			this.getMarkup();
			var that = this;

			jQuery( 'body' ).append( this.markup );

			if( this.fixed ){

				jQuery( this.selector ).hide();

				var windowHeight = jQuery( window ).height();

				jQuery( window ).scroll(function(){

					var scrollPos     = jQuery( window ).scrollTop();

					if(  ( scrollPos > ( windowHeight - 100 ) ) ){

						if( false == that.visible ){
							jQuery( that.selector ).fadeIn();
							that.visible = true;
						}
						
					}else{

						if( true == that.visible ){
							jQuery( that.selector ).fadeOut();
							that.visible = false;
						}
					}
				});

				this.bindEvent();
			}
		}
	}

	this.bindEvent = function(){
		jQuery( document ).on( 'click', this.selector, function(e){
			e.preventDefault();
			$("html, body").animate({ scrollTop: 0 }, 800);
		});
	}

	this.getMarkup = function(){

		var position = this.fixed ? 'fixed':'absolute';

		var wrapperStyle = 'style="position: '+position+';z-index:999999; bottom: 20px; right: 20px;"';

		var buttonStyle  = 'style="cursor:pointer;display: inline-block;padding: 10px 20px;background: #f15151;color: #fff;border-radius: 2px;"';

		var markup = '<div ' + wrapperStyle + ' id="scroll-to-top"><span '+buttonStyle+'>Scroll To Top</span></div>';

		this.markup   = ( param && param.markup ) ? param.markup : markup;
	}

	this.valid = function(){

		if( param && param.markup && !param.selector ){
			alert( 'Please provide selector. eg. { markup: "<div id=\'scroll-top\'></div>", selector: "#scroll-top"}' );
			return false;
		}

		return true;
	}
}

function fixedNav( param ){

	this.menu      = param.menu;
	this.newClass  = null;
	this.position  = null;
	this.top       = null;
	this.init = function(){

		this.newClass = param.newClass ? param.newClass : 'fixed-nav-active';
		this.position = $(this.menu).css( "position" );
		this.top      = $(this.menu).css( 'top' );

		this.deploy();
		var that = this;
		$(window).scroll(function(){

			that.deploy();

		});
	}

	this.deploy = function(){

		var menuHeight    = $(this.menu).height();
		var scrollPos     = $(window).scrollTop();

		if( scrollPos >= menuHeight ){

			if( !$(this.menu).hasClass( this.newClass ) ){

				$(this.menu).css({
					'position' : 'fixed',
					'top'	   : '0' 	
				});

				$(this.menu).addClass( this.newClass );
			}
		}else{

			if( $(this.menu).hasClass( this.newClass ) ){

				$(this.menu).css({
					'position' : this.position,
					'top'	   : this.top
 				});

				$(this.menu).removeClass( this.newClass );
			}
		}
	}
}

function preloader( param ){

	this.time = param.time;

	this.init = function(){

		var time = this.time;

		$(window).load( function(){
			$( 'body' ).animate({
				opacity : 1
			},time);
		})
	}
}

function mixitSearch( param ){

	this.header    = param.header,
	this.toggler   = param.toggler,
	this.searchEle = param.searchEle,
	this.enabled   = false,
	this.init      = function(){
		var that = this;

		$('.mixit-search').on('click', function(e){
			e.stopPropagation();
			that.enabled = true;
			$( that.header ).slideUp(100);
			$( that.searchEle ).slideDown(200);
		});

		$( document ).on('click',function(e){
			if ( $(e.target).closest(that.searchEle).length === 0 && that.enabled ) {
		        that.enabled = false;
				$( that.header ).slideDown(100);
				$( that.searchEle ).slideUp(200);
		        //
		    }
		});
	}
}

function lazyReload( param ){

	this.time = param.time;

	this.init = function(){

		var time = this.time;

		$(document).on( 'click', '.mixit-menu-wrapper a', function(e){

			e.preventDefault();
			
			var path = $(this).attr('href');
				
			if( typeof path != 'undefined' && path.length > 0 && path.indexOf( '#' ) == -1 ){

				setTimeout( function(){
					window.location.href= path;

				},time);

				$('body').animate({
					opacity : 0
				},time);
			}
		})
	}
}

function load_product_details(){

	this.loaderClass = 'loading';

	this.init = function(){

		var that = this;

		jQuery( '.quickview' ).on( 'click',function( e ){

			e.preventDefault();

			that.ele = jQuery( this );
			
			that.showLoader();

			that.productId = jQuery( this ).attr( 'data-product-id' );
			
			if( !isNaN( that.productId ) ){
				that.ajax();
			}else{
				that.removeLoader();
			}
		});
	}

	this.showLoader = function(){
		this.ele.addClass( this.loaderClass );
	}

	this.removeLoader = function(){
		this.ele.removeClass( this.loaderClass );
	}

	this.ajax = function(){

		var that = this;

		var onPost =  function( data ){

	    	var dt = data;

	    	jQuery( '#dialog-product #content .product').html( dt.data );

	    	var options = {
			    autoOpen: false,
			    dialogClass: "no-title",
			    height: $(window).height() - 50,
			    width: '90%',
			    fluid: true, //new option
    			resizable: false, modal: true,
    			open : function(event, ui){

				    $("body").css({
				        overflow: 'hidden'
				    });

				    $(".ui-widget-overlay").css({
				        background:"rgb(0, 0, 0)",
				        opacity: ".50 !important",
				        filter: "Alpha(Opacity=50)",
				    });
				},
				beforeClose: function(event, ui) {
				    $("body").css({ overflow: 'inherit' })
				},
			    hide: {
		            effect: "scale",
		            easing: "easeInBack"
			        },
		        buttons: {
			        Cancel: function() {
			          dialog.dialog( "close" );
			        }
			    },
			    closeOnEscape: true,
		        show: {
		            effect: "scale",
		            easing: "easeOutBack"
		        }
			}

	    	var dialog = $( "#dialog-product" ).dialog( options );
					 
			dialog.dialog( "open" );

			$easyZoom = jQuery('.easyzoom').easyZoom();
			cpt.easyZoomTearDown();
			productThumbnailSlider( true );
			//var $easyzoom = jQuery('.easyzoom').easyZoom();
		}

		var options = { 'action': 'mixit_load_product_details', 'data':   { 'product_id' : this.productId } };

		var onComplete = function(){
		    that.removeLoader();
		}

		$.ajax({ 
			url: self.ajaxurl,
			type: "POST",
			data: { 'action':'mixit_load_product_details','product_id' : this.productId },
			success: onPost,
			dataType: "JSON",
			complete: onComplete 
		});
	}
}

//quick & simple Accordion for categories widget
//quick & simple Accordion for categories widget
function productCategoriesAccordion(){
        
    var $children     = $( 'ul.product-categories .children' );
    var plus          = '<i class="fa fa-plus"></i>';
    var minus         = '<i class="fa fa-minus"></i>';
    var openClass     = 'cat-open';
    var $parentAnchor = $( 'ul.product-categories .cat-parent > a' );

    $parentAnchor.append( plus );

    $children.hide();

    $parentAnchor.on( 'click',function( e ){

        e.preventDefault();

        //************ Appending hash url ********* //

        var $parentListItem = $( this ).parent( 'li' );

        var myClass;

        var classNames = $parentListItem.attr('class').split(/\s+/);  

         $.each(classNames, function(index, item) {
            if(item.indexOf("cat-item-") == 0){
              myClass = item;
            }
         });

        window.location.hash = '#'+myClass;

        // ******************************* //

        var $thisChildren = $( this ).nextAll( '.children' );

        $children.not( $thisChildren ).slideUp();

        $parentAnchor.not( this ).removeClass( openClass );

        $( this ).toggleClass( openClass );
        
        $parentAnchor.not( this ).find( 'i').remove();
        $parentAnchor.not( this ).append( plus );

        if( $( this ).hasClass( openClass ) ){

            $( this ).find( 'i.fa-plus' ).remove();
            $( this ).append( minus );
        }else{

            $( this ).find( 'i.fa-minus' ).remove();
            $( this ).append( plus );
        } 

        $thisChildren.slideToggle();

        var removeHash = true;

        $.each( $parentAnchor, function(){
            if( $(this).hasClass( 'cat-open' ) ){
                removeHash = false;
            }
        });

        if( removeHash ){
            window.location.hash = '';
        }
    
    });

    // ********* open the previously opened category ************ //
    var currentElement = window.location.hash.replace( '#', '.').trim();
    
    if( '' != currentElement ){
        $( currentElement + ' a' ).trigger( 'click' );
    }
}

function changeProductThumbnail(){

	this.toggler = '.product-img-thumb';
	this.wrapper = '.easyzoom';

	this.apply = function(){

		var that = this;
		$easyZoom = jQuery('.easyzoom').easyZoom();

		jQuery( document ).on( 'click', this.toggler, function( e ){

			e.preventDefault();
			
			$( that.toggler ).not( this ).removeClass( 'active-img' );
			$( this ).addClass( 'active-img' );

			var largeImg = jQuery( this ).attr( 'data-large-img' );
			var mainImg  = jQuery( this ).attr( 'data-main-img' );

			var targetImg = jQuery( that.wrapper ).find( 'img' );
			var targetAnch = jQuery( that.wrapper ).find( 'a' );

			targetImg.attr( 'src', mainImg );
			targetImg.removeAttr( 'srcset' ).removeAttr( 'sizes' );
			targetAnch.attr( 'href', largeImg );

			that.easyZoomTearDown();
		});
	}

	this.easyZoomTearDown = function(){
		var api = $easyZoom.data('easyZoom');

		if( 'undefined' !== typeof api ){
			api.teardown();
		}

		jQuery(this.wrapper).easyZoom();
	}
}

// Pop up after add to cart
function addedToCartPopUp(){

	this.dialogInstance = null;
	this.wrapper = "#popup-after-add-to-cart";
	this.title   = '#added-to-cart-name';
	this.counter = this.wrapper + ' .close-message-text i';
	
	this.init = function(){
		var that =  this;

		$( document.body ).on( 'added_to_cart', function( event, fragments, cart_hash, $button ) {

			var $parent = $button.parents( 'li' ),
				img_url = $parent.find('img').attr( 'src' ),
				title   = $parent.find( '.product-item-info h3 a' ).text();

			$( that.wrapper + ' img' ).attr( 'src', img_url );
			$( that.title ).text( title );
			that.dialogInstance = $( that.wrapper ).dialog({
						      autoOpen: false,
						      dialogClass: "added-cart-no-title",
						      height: 400,
						      width: 500,
						      open : function(event, ui){

								    $("body").css({
								        overflow: 'hidden'
								    });

								    $(".ui-widget-overlay").css({
								        background:"rgb(0, 0, 0)",
								        opacity: ".50 !important",
								        filter: "Alpha(Opacity=50)",
								    });
								},
								beforeClose: function(event, ui) {
								    $("body").css({ overflow: 'inherit' })
								},
						      hide: {
					            effect: "scale",
					            easing: "easeInBack"
					        },
					        closeOnEscape: true,
					        show: {
					            effect: "scale",
					            easing: "easeOutBack"
					        }
						    });
						 
			that.dialogInstance.dialog( "open" );
			that.interval();

		});

		$( document ).on( 'click', function(){

			if( null == that.dialogInstance )
				return;
			that.dialogInstance.dialog( "close" );
		});
	}

	this.interval = function(){

		var count = 0,
			max = 10,
			time = 1,
			that = this;

		var interval = setInterval( function(){

			$( that.counter ).text( ( max - time ) - count );
			
			if( max == count ){
				clearInterval( interval );
				that.dialogInstance.dialog( 'close' );
			}

			count++;

		},( time * 1000 ));
	}
}

function productThumbnailSlider( isPopUp ){

	var element = 'product-thumbnail-caroufredsel';

	if( null == document.getElementById( element ) ){
		return;
	}

	if( typeof isPopUp == 'undefined' && $('#'+element + ' .product-img-thumb').length < 5 ){
		return;
	}

	if( typeof isPopUp != 'undefined' && $('#'+element + ' .product-img-thumb').length < 4 ){
		return;
	}

	var $highlight = function() { 
	    var $this = $("#" + element);

	    var items = $this.triggerHandler("currentVisible");     //get all visible items
	    $this.children().removeClass("item-visible");                 // remove all .active classes
	    items.addClass("item-visible");              // add .active class to n-th item
	};

	var options = {
		responsive: true,
		circular: true,
		infinite: false,
		auto 	: false,
		width: "100%",
		items : {
            visible: {
            	min: 1,
            	max: 4
            }
		},
		prev	: {
			button	: "#product-thumb-prev",
			key		: "left"
		},
		next	: {
			button	: "#product-thumb-next",
			key		: "right"
		}
	}

	jQuery( '.product-thumbnail-paginattion' ).css( { 'display': 'block' } );

	jQuery( '#' + element ).carouFredSel( options );
}

$(document).ready(function(){

	cpt = new changeProductThumbnail();
	cpt.apply();

	$("html").niceScroll({
		smoothscroll : true,
		mousescrollstep : 100,
		zindex: 999999 
	});

	// Convert Image inside Deisgner Main to background Image
	$(".designer-main").each(function() {
	    $(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") center center /cover' );
	    $(this).find('img').css('display', 'none');
	});

	$(".blog .post-image").each(function() {
	    $(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") center center /cover' );
	    $(this).find('img').css('display', 'none');
	});

	/* if is below 481px */
	var pagebanner = function(){
		var responsive_viewport = $(window).width();
		if (responsive_viewport <= 767) {
			$(".page-banner-image").each(function() {
			    $(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") center center /cover' );
			    $(this).find('img').css('display', 'none');
			});
		} else{
			$(".page-banner-image").each(function() {
				$(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") center center /cover' );
			    $(this).find('img').css({'display': 'block', 'opacity': '0', 'visibility': 'hidden'});
			});
		}
	}
	pagebanner();
	$(window).resize(function(){
		pagebanner();
	});

	$('#filtertoggle').on('click', function(e){
		e.preventDefault();
	    $(document).find('.shop-sidebar').toggleClass('hidden-xs');
	    $(document).find('.shop-sidebar');
	    $(this).toggleClass('closefilter');
	});
	
	new addedToCartPopUp().init();

	/***** Commented for now *****/
	
	// new load_product_details({
	// }).init();

	new scrollToTop({
		markup   : '<div id="scroll-to-top"><span><i class="fa fa-angle-double-up"></i></span></div>',
		selector : '#scroll-to-top'
	}).init();

	new fixedNav({
		menu     : '#header'
	}).init();

	new lazyReload({
		time : 500
	}).init();

	new preloader({
		time : 1000
	}).init();

	new mixitSearch({
		header  : '#header',
		toggler : '#.mixit-search',
		searchEle : '#search-wrapper'
	}).init();

	$('[data-toggle="tooltip"]').tooltip();

	//$("#mixit-mmenu").mmenu();

	$( document.body ).on( 'added_to_cart', function( event, fragments, cart_hash ) {

		var $cartCounter = $('#cart-item-count');

		var count = parseInt( $cartCounter.text() );

		count =  isNaN( count ) ? 0 : count;
		
		$cartCounter.text( ++count ); 
		$( '#mixit-cart-detail' ).html(fragments["div.widget_shopping_cart_content"]);

	});

	$( window ).scroll(function(){

		var h 			= $( '.page-banner-image' ).outerHeight();
		var scrollPos   = $(window).scrollTop();

		if( scrollPos >= h ){
			$( '#header' ).addClass( 'scroll-header' );
		}else{
			$( '#header' ).removeClass( 'scroll-header' );
		}

	})

	productCategoriesAccordion();
});

$(window).load( function(){

	productThumbnailSlider();

	$("#mixit-homepage-caroufredsel").carouFredSel({
        auto: true,
        infinite: true,
        responsive: true,
        width: '100%',
        items: {
            visible: {
                min: 1,
                max: 7
            }
        },
        scroll:1,
        swipe       : {
			onTouch : true,
			onMouse: true
		}
		// pagination	: "#mixit-homepage-caroufredsel-pagination"
    });

	var $grid = $('.grid').isotope({
	  filter : '*'
	});

	// filter items on button click
	$('.filter-button-group').on( 'click', 'button', function() {
	  var filterValue = $(this).attr('data-filter');
			$(this).addClass('active');
	  		$(this).siblings().removeClass('active');
	  $grid.isotope({ filter: filterValue });
	});

	var $videoBanner = $('#banner-video');
	if( typeof $videoBanner != 'undefined' && typeof $videoBanner.get( 0 ) != 'undefined' ){
		$videoBanner.get(0).play();
	}


	/*Cart Custom Checkout Button*/
    /*jQuery('form[name=mixit-checkout-select]').on('submit', function (e) {
        e.preventDefault();
        window.location.href = jQuery("input[name=payment]:checked").val();
    });
*/


/* if checkout page */
// jQuery(window).load( function($){
	setTimeout(function(){

		 if( jQuery('body.checkout').length > 0 && jQuery('input#payment_method_cart').val() !=''  ) {
		 	var woo_chosen = jQuery('input#payment_method_cart').val();
		 	if( jQuery('input[name="payment_method"][value="' + woo_chosen+ '"]').attr('checked',true) ){		 		
		 		if ("ecster" === $("input[name='payment_method']:checked").val() ) {		 		
					$("body").addClass("ecster-selected").removeClass("ecster-deselected");
			 		$("body").trigger("update_checkout");
				} else {		 		
					$("body").removeClass("ecster-selected").addClass("ecster-deselected");
				}
			 }
		 	}
	},1000);

	/*Home scroll top*/
	jQuery('.banner-arrow').on('click', function(e){
		var target = jQuery('.designer-template-wrapper'),
			header = jQuery('#header').height();
		$("html, body").animate({ 
			scrollTop: parseInt(target.position().top - header)
		}, "slow");

	});

	if( jQuery('.page-banner-image').length > 0 ){
		setTimeout(function(){
			jQuery('.page-banner-image img').css( 'height', jQuery(window).height() - (20 * jQuery(window).height() /100) );
		},500);
	}
	

	jQuery(document).on('click', '.imapper-pin-wrapper-my-inner-front',  function(){ 
		// jQuery('.imapper-content').css( 'width','100%' );
		jQuery('.site-header').css( 'width','100%' );
		
	});
		

});

