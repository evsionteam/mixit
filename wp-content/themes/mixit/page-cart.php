<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : 
				the_post(); 
				$post_id = get_the_title();
		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php get_template_part( 'template-parts/content', 'banner' ); ?>

			<div class="container">
				<div class="cart-table">
					<?php the_content(); ?>
				</div>
			</div><!-- .container -->
		</article>
		
	<?php endwhile;  ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
