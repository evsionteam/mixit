<?php
/**
 * Template Name: Brand Page
 */
get_header();

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php get_template_part( 'template-parts/content', 'banner' ); ?>
	<div class="designer-template-wrapper">
		<div class="container">

			<div class="brand-title"><?php echo get_post_meta( get_the_ID(), 'page_subtitle', true ); ?></div>
			<div class="brands-wrapper">
				<?php 
					$terms = get_terms( array(
					    'taxonomy'   => 'brand',
					    'hide_empty' => false,
					    'orderby'    => 'name'
					) );
				
					if( !is_wp_error( $terms ) && count( $terms ) > 0 ):

						foreach( $terms as $key => $val ) {

							$char = trim($val->name)[0];

							if ( !ctype_alpha( $char ) ){
								$char = '#';
							}

							if ( $char !== $lastChar ) {

								if( $key != 0 ){
									echo '</ul>';
								}

								if ( $lastChar !== '' ){
									echo '<ul class="brands">';
								}

								echo '<li class="head">' . strtoupper($char).'</li>';
								$lastChar = $char;
							}

							echo '<li><a href="'.get_term_link( $val, 'brand' ).'">'.$val->name.'</a></li>';
						}

						echo '</ul>';
					endif;
				?>
			</div>
		</div><!-- .container -->
	</div>
</article>
<?php get_footer(); 