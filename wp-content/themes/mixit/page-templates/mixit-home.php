<?php
/**
 * Template Name: Home Page
 */
	get_header();
	global $mixit_option;
	$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
	$cart_page_url = get_permalink( woocommerce_get_page_id( 'cart' ) );
?>

<div class="main-content">
	<div class="banner-holder page-banner-image2">
		
			<video preload="none"  width="100%" poster="<?php echo $mixit_option[ 'video_poster' ]['url']; ?>" autoplay loop id="banner-video" style="background-image: url(<?php echo $mixit_option[ 'video_poster' ]['url']; ?>); -webkit-background-size: cover; background-size: cover;" muted>
			<source src="<?php echo $mixit_option['video_mp4']; ?>" type="video/mp4">
			<source src="<?php echo $mixit_option['video_webm']; ?>" type="video/webm">
			<source src="<?php echo $mixit_option['video_ogg']; ?>" type="video/ogg">
			Your browser does not support the video tag.
		</video>

		
		<div class="overlay doted-overlay"></div>
		<div class="banner-text">
			<div class="welcome-text"><?php echo $mixit_option['video_subtitle']; ?></div>
			<h1><?php echo $mixit_option['video_title']; ?></h1>
			<div class="slogan"><?php echo $mixit_option['video_description']; ?></div>
			<a href="<?php echo $mixit_option[ 'video_btn_1_link' ]; ?>" class="c-btn-pri btn-shop-now">
				<?php echo $mixit_option[ 'video_btn_1_text' ]; ?>
			</a>
			<a href="<?php echo $mixit_option[ 'video_btn_2_link' ]; ?>" class="btn-we">
				<?php echo $mixit_option[ 'video_btn_2_text' ]; ?>
			</a>
		</div>
	</div>
</div>


<!-- Designer Listing Template -->
<div class="designer-template-wrapper">
	<div class="title-designer text-center">
		<h1><?php echo $mixit_option[ 'designer_title' ]; ?></h1>
		<p><?php  echo $mixit_option[ 'designer_description' ]; ?></p>
	</div>

		<div class="designer-grid">

					<div class="designer-holder">
				
<p>Jessica</p>
<p>Marina</p>
<p>Madde</p>
<p>Eva</p>
<p>Barbara</p>


			</div>
		
	</div>

	<div class="clearfix"></div>





<div class="products">

<div class="title-designer text-center">
		<h1>Senaste inkommet</h1>
		<p>Här kan du se våra senaste produkter vi har på webben.</p>
	</div>

	
	<div class="">
		

		<?php
            $product_cats = get_terms(array(
                'taxonomy' => 'product_cat',
                'hide_empty' => true,
                'parent' => 0
            ));
            if (!empty($product_cats)):
            	echo '<div class="button-group filter-button-group">';
        ?>
            	<button class="c-tab-btn active" data-filter="*"><?php _e( 'Show All','mixit' ); ?></button>
            <?php
                foreach ($product_cats as $product_cat):
                    ?>
                    <button class="c-tab-btn" data-filter=".<?php echo $product_cat->slug; ?>">
                        <?php echo $product_cat->name; ?>
                    </button>
                    <?php
                endforeach;
                echo '</div>';
            endif;
        	?>

        <?php
			$unique_products = [];
            if (!empty($product_cats)):
            	echo '<ul class="products grid">';
                foreach ($product_cats as $product_cat):
                    /*
                     * set sk_cat_slug to use in home-products template part
                     * use $sk_cat_slug to get the value
                     * */
                    set_query_var('sk_cat_slug', $product_cat->slug);
                    get_template_part('template-parts/home', 'products');
                endforeach;
                echo '</ul>';
            endif;
        ?>
	</div>
	<div class="mixit-product-button" >
		
		<a href="<?php echo $shop_page_url ?>">
			<?php _e( 'VIEW MORE PRODUCTS', 'mixit' ); ?>
			<i class="fa fa-angle-right"></i>
		</a>
	</div>
</div>


<!-- Shop Brand Wrapper -->
	<div class="shop-by-brand-wrapper text-center">
		<div class="mixit-brand_carousel">
			<div id="mixit-homepage-caroufredsel" >
				<?php 

					$terms = get_terms( array(
					    'taxonomy' => 'brand',
					    'hide_empty' => false,
					));

					foreach( $terms as $t ):
						$t_id = $t->term_id;
						$term_meta = get_option( "brand_$t_id" );
						$brandimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : ''; 
						if( $brandimage == '') {continue;}
						$link = get_term_link( $t_id, 'brand' );
						$target = get_term_meta( $t_id , 'target', true );		
				?>
					<a class='brand-item' href="<?php echo $link;?>" target="<?php echo ($target!='') ? $target : '_self';?>" style="background-image: url('<?php echo $brandimage; ?>');background-size: 95%;background-position: center center;display: inline-block;" ></a>
				<?php		
					endforeach;
				?>
				
			</div>
		</div>

		<div id="mixit-homepage-caroufredsel-pagination" class="shop-brand-pagination"></div>
	</div>
	
</div>




<!-- Above the footer -->

<div class="customer-block">
	<?php if ( is_active_sidebar( 'above-footer' ) ) : ?>
			<?php dynamic_sidebar( 'above-footer' ); ?>
	<?php endif; ?>
	
</div>

<!-- Above the footer -->

<?php get_footer(); ?>


