<?php
/**
 * Template Name: Home Page ( New )
 */
	get_header();
	global $mixit_option;
?>

<div class="main-content">
	<div class="banner-holder designer-banner-holder">
		<video preload="none"  width="100%" poster="<?php echo $mixit_option[ 'video_poster' ]['url']; ?>" autoplay loop id="banner-video" style="background-image: url(<?php echo $mixit_option[ 'video_poster' ]['url']; ?>); -webkit-background-size: cover; background-size: cover;" muted>
			<source src="<?php echo $mixit_option['video_mp4']; ?>" type="video/mp4">
			<source src="<?php echo $mixit_option['video_webm']; ?>" type="video/webm">
			<source src="<?php echo $mixit_option['video_ogg']; ?>" type="video/ogg">
			Your browser does not support the video tag.
		</video>

		<div class="overlay doted-overlay"></div>
		
		<div class="banner-text">
			<h1><?php echo $mixit_option['video_title']; ?></h1>
			<div class="slogan"><?php echo $mixit_option['video_subtitle'];?></div>
		</div>
	</div>
		<div class="banner-arrow"><div class="arrow-line"></div></div>
</div>

<!-- Designer Listing Template -->
<div class="designer-template-wrapper">
	<div class="title-designer text-center">
		<h1><?php echo $mixit_option[ 'designer_title' ]; ?></h1>
		<p><?php  echo $mixit_option[ 'designer_description' ]; ?></p>
	</div>

	<?php
		$featured_designer_id = get_option( 'featured_designer' );
		$args = array(
			'post_type'        => 'designer',
			'exclude'          => $featured_designer_id,
			'posts_per_page'   => 4,
			'post_status'      => 'publish',
			'orderby'          => 'date',
			'order'            => 'DESC',
		);
		$designers = get_posts( $args );
		$post = get_post( $featured_designer_id );
	?>
	<div class="designer-grid">

		<?php if( $post ): setup_postdata( $post );  ?>
			<div class="designer-holder">
				<div class="designer-main">
					<a href="<?php the_permalink(); ?>">
						<img src="<?php the_post_thumbnail_url( 'featured-designer' ); ?>" alt="google-image">
						<div class="hover-designer"></div>
						<p><?php the_title(); ?></p>
					</a>
				</div>
			</div>
		<?php endif; ?>

		<?php foreach ( $designers as $post ): setup_postdata( $post );  ?>

			<?php if( has_post_thumbnail() ): ?>
				<div class="designer-holder">
					<div class="designer-main">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php the_post_thumbnail_url( 'designer' );  ?>" alt="google-image">
							<div class="hover-designer"></div>
							<p><?php the_title(); ?></p>
						</a>
					</div>
				</div>

			<?php endif; ?>
		<?php endforeach; wp_reset_postdata(); ?>
	</div>

	<div class="clearfix"></div>

	<!-- Shop Brand Wrapper -->
	<div class="shop-by-brand-wrapper text-center">
		<div class="mixit-brand_carousel">
			<div id="mixit-homepage-caroufredsel" >
				<?php 

					$terms = get_terms( array(
					    'taxonomy' => 'brand',
					    'hide_empty' => false,
					));

					foreach( $terms as $t ):
						$t_id = $t->term_id;
						$term_meta = get_option( "brand_$t_id" );
						$brandimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : ''; 
						if( $brandimage == '') {continue;}
						$link = get_term_link( $t_id, 'brand' );
						$target = get_term_meta( $t_id , 'target', true );		
				?>
					<a class='brand-item' href="<?php echo $link;?>" target="<?php echo ($target!='') ? $target : '_self';?>" style="background-image: url('<?php echo $brandimage; ?>');background-size: 95%;background-position: center center;display: inline-block;" ></a>
				<?php		
					endforeach;
				?>
				
			</div>
		</div>

		<div id="mixit-homepage-caroufredsel-pagination" class="shop-brand-pagination"></div>
	</div>
	
</div>


<!-- Above the footer -->
<div class="customer-block">
	<?php if ( is_active_sidebar( 'above-footer' ) ) : ?>
			<?php dynamic_sidebar( 'above-footer' ); ?>
	<?php endif; ?>
	
</div>

<!-- Above the footer -->

<?php get_footer(); ?>


