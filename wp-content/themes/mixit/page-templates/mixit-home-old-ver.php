<?php
/**
 * reTemplate Name: Home Page
 */
get_header();
global $mixit_option;
?>
<div class="main-content">
	<div class="banner-holder">
		
			<video  width="100%" poster="<?php echo $mixit_option[ 'video_poster' ]['url']; ?>" autoplay loop id="banner-video">
				<source src="<?php echo $mixit_option['video_mp4']; ?>" type="video/mp4">
				<source src="<?php echo $mixit_option['video_webm']; ?>" type="video/webm">
				<source src="<?php echo $mixit_option['video_ogg']; ?>" type="video/ogg">
				Your browser does not support the video tag.
			</video>
		
		<div class="overlay doted-overlay"></div>
		<div class="banner-text">
			<div class="welcome-text"><?php echo $mixit_option['video_subtitle']; ?></div>
			<h1><?php echo $mixit_option['video_title']; ?></h1>
			<div class="slogan"><?php echo $mixit_option['video_description']; ?></div>
			<a href="<?php echo $mixit_option[ 'video_btn_1_link' ]; ?>" class="c-btn-pri btn-shop-now">
				<?php echo $mixit_option[ 'video_btn_1_text' ]; ?>
			</a>
			<a href="<?php echo $mixit_option[ 'video_btn_2_link' ]; ?>" class="btn-we">
				<?php echo $mixit_option[ 'video_btn_2_text' ]; ?>
			</a>
		</div>
	</div>
</div>

<div class="products">
	<div class="">
		<h2><?php echo $mixit_option['product_title'] ?></h2>

		<?php
            $product_cats = get_terms(array(
                'taxonomy' => 'product_cat',
                'hide_empty' => true,
                'parent' => 0
            ));
            if (!empty($product_cats)):
            	echo '<div class="button-group filter-button-group">';
        ?>
            	<button class="c-tab-btn active" data-filter="*"><?php _e( 'Show All','mixit' ); ?></button>
            <?php
                foreach ($product_cats as $product_cat):
                    ?>
                    <button class="c-tab-btn" data-filter=".<?php echo $product_cat->slug; ?>">
                        <?php echo $product_cat->name; ?>
                    </button>
                    <?php
                endforeach;
                echo '</div>';
            endif;
        	?>

        <?php
			$unique_products = [];
            if (!empty($product_cats)):
            	echo '<ul class="products grid">';
                foreach ($product_cats as $product_cat):
                    /*
                     * set sk_cat_slug to use in home-products template part
                     * use $sk_cat_slug to get the value
                     * */
                    set_query_var('sk_cat_slug', $product_cat->slug);
                    get_template_part('template-parts/home', 'products');
                endforeach;
                echo '</ul>';
            endif;
        ?>
	</div>
	<div class="mixit-product-button" >
		<?php $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>
		<a href="<?php echo $shop_page_url ?>">
			<?php _e( 'VIEW MORE PRODUCTS', 'mixit' ); ?>
			<i class="fa fa-angle-right"></i>
		</a>
	</div>
</div>

<div class="shop-by-brand-wrapper text-center">
	<h3 class="title"><?php echo $mixit_option['shop_by_brand_title'] ?></h3>
	<div class="description"><?php echo $mixit_option['shop_by_brand_description']; ?></div>
	<div class="mixit-brand_carousel">
		<div id="mixit-homepage-caroufredsel" >
			<?php 

				$terms = get_terms( array(
				    'taxonomy' => 'brand',
				    'hide_empty' => false,
				));

				foreach( $terms as $t ):
					$t_id = $t->term_id;
					$term_meta = get_option( "brand_$t_id" );
					$brandimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : ''; 
			?>
				<div class='brand-item' style="background-image: url('<?php echo $brandimage; ?>')" ></div>
			<?php		
				endforeach;
			?>
			
		</div>
	</div>

	<div id="mixit-homepage-caroufredsel-pagination" class="shop-brand-pagination"></div>
</div>

<div class="offer">
	<div class="box-column">
		<div class="box">

				<?php if( !empty( $mixit_option[ 'cta_2_image']['url']) ){ 
					$background_img =  "style=\"background-image:url('" .$mixit_option[ 'cta_2_image']['url']. "\")"; 
				} $background_img = '';?> 
			
			<style>
				.bg-img-1{
					background-image : url('<?php echo  $mixit_option[ 'cta_2_image']['url']; ?>');
				}

				.bg-img-2{
					background-image : url('<?php echo  $mixit_option[ 'cta_2_image']['url']; ?>');
				}
			</style>

			<div class="background-img bg-img-1">
			</div>

			<!-- overlay -->
			<div class="overlay"></div>

			<div class="box-holder">
				<div class="box-text">
					<h2><?php echo $mixit_option[ 'cta_2_subtitle' ]; ?></h2>
					<div class="sub-title"><?php echo $mixit_option[ 'cta_2_title' ]; ?></div>
					<p><?php echo $mixit_option[ 'cta_2_description' ]; ?></p>
			          <a href="<?php echo $mixit_option[ 'cta_2_btn_link' ]; ?>" class="box-btn">
			          	<?php echo $mixit_option[ 'cta_2_btn_text' ]; ?>
			          </a>
				</div><!-- box-text -->
			</div><!-- box-holder -->
		</div><!-- box -->

	</div><!-- box-column -->

	<div class="box-column">
		<div class="box">

				<?php if( !empty( $mixit_option[ 'cta_2_image']['url']) ){ 
					$background_img =  "style=\"background-image:url('" .$mixit_option[ 'cta_2_image']['url']. "\")"; 
				}  $background_img = ''; ?> 

			<div class="background-img bg-img-2">
			</div>

			<!-- overlay -->
			<div class="overlay"></div>

			<div class="box-holder">
				<div class="box-text">
					<h2><?php echo $mixit_option[ 'cta_2_subtitle' ]; ?></h2>
					<div class="sub-title"><?php echo $mixit_option[ 'cta_2_title' ]; ?></div>
					<p><?php echo $mixit_option[ 'cta_2_description' ]; ?></p>
			          <a href="<?php echo $mixit_option[ 'cta_2_btn_link' ]; ?>" class="box-btn">
			          	<?php echo $mixit_option[ 'cta_2_btn_text' ]; ?>
			          </a>
				</div><!-- box-text -->
			</div><!-- box-holder -->
		</div><!-- box -->

	</div><!-- box-column -->
</div><!-- offer -->

<!-- Above the footer -->

<div class="customer-block">
	<?php if ( is_active_sidebar( 'above-footer' ) ) : ?>
			<?php dynamic_sidebar( 'above-footer' ); ?>
	<?php endif; ?>
	
</div>

<!-- Above the footer -->

<?php get_footer(); ?>


