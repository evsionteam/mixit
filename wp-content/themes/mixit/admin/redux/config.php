<?php
    
    if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxCore/framework.php' ) ) {
        require_once( dirname( __FILE__ ) . '/ReduxCore/framework.php' );
    }

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $mixit_option = "mixit_option";

    $args = array( 
                'menu_title' => 'Mixit Options', 
                'page_title' => 'Mixit Options', 
                'save_defaults' => 'true'
            );

    Redux::setArgs ($mixit_option, $args);
    /* General Settings */

    Redux::setSection( $mixit_option, array(
        'title'      => __( 'General Settings', 'mixit' ),
        'id'         => 'general',
        'fields'     => array(
            array(
                'id'       => 'site_title',
                'type'     => 'text',
                'title'    => __( 'Site Title', 'mixit' ),
                'default'  => 'Mixit - Online Shopping Portal'
            ),array(
                'id'       => 'designer_title',
                'type'     => 'text',
                'title'    => __( 'Designer Title', 'mixit' )
            ),array(
                'id'       => 'designer_description',
                'type'     => 'textarea',
                'title'    => __( 'Designer Description', 'mixit' )
            ),array(
                'id'       => 'site_banner',
                'type'     => 'media',
                'title'    => __( 'Site Banner', 'mixit' )
            ),array(
                'id'       => 'product_title',
                'type'     => 'text',
                'title'    => __( 'Product Title', 'mixit' ),
                'default'  => 'Nyheter'
            ),array(
                'id'       => 'header_logo',
                'type'     => 'media',
                'title'    => __( 'Header Logo', 'mixit' )
            ),array(
                'id'       => 'footer_logo',
                'type'     => 'media',
                'title'    => __( 'Footer Logo', 'mixit' )
            ),array(
                'id'       => 'related_product_description',
                'type'     => 'textarea',
                'title'    => __( 'Related Product Description', 'mixit' )
            )
        )
    ));

    Redux::setSection( $mixit_option, array(
        'title'      => __( 'Video', 'mixit' ),
        'id'         => 'video',
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'       => 'video_poster',
                    'type'     => 'media',
                    'title'    => __( 'Poster', 'mixit' )
                ),array(
                    'id'       => 'video_mp4',
                    'type'     => 'text',
                    'title'    => __( 'mp4', 'mixit' )
                ),array(
                    'id'       => 'video_webm',
                    'type'     => 'text',
                    'title'    => __( 'webm', 'mixit' )
                ),array(
                    'id'       => 'video_ogg',
                    'type'     => 'text',
                    'title'    => __( 'ogg', 'mixit' )
                ),array(
                    'id'       => 'video_title',
                    'type'     => 'text',
                    'title'    => __( 'Title', 'mixit' )
                ),array(
                    'id'       => 'video_subtitle',
                    'type'     => 'textarea',
                    'title'    => __( 'Sub Title', 'mixit' )
                ),array(
                    'id'       => 'video_desc',
                    'type'     => 'text',
                    'title'    => __( 'Description', 'mixit' )
                ),array(
                    'id'       => 'video_btn_1_text',
                    'type'     => 'text',
                    'title'    => __( 'Button 1 text', 'mixit' )
                ),array(
                    'id'       => 'video_btn_1_link',
                    'type'     => 'text',
                    'title'    => __( 'Button 1 link', 'mixit' )
                ),array(
                    'id'       => 'video_btn_2_text',
                    'type'     => 'text',
                    'title'    => __( 'Button 2 text', 'mixit' )
                ),array(
                    'id'       => 'video_btn_2_link',
                    'type'     => 'text',
                    'title'    => __( 'Button 2 link', 'mixit' )
                )

            ),
        
    ));

    Redux::setSection( $mixit_option, array(
        'title' => __( 'Brands', 'mixit' ),
        'id'    => 'brands',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => 'brand_title',
                'type'      => 'text',
                'title'     => __( 'Title', 'mixit' )
            ),
            array(
                'id'        => 'brand_description',
                'type'      => 'textarea',
                'title'     => __( 'Description', 'mixit' )
            )
        )
    ) );

    Redux::setSection( $mixit_option, array(
        'title'      => __( 'Shop by Brand', 'mixit' ),
        'id'         => 'shop-by-brand',
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'       => 'shop_by_brand_title',
                    'type'     => 'text',
                    'title'    => __( 'Title', 'mixit' )
                ),array(
                    'id'       => 'shop_by_brand_description',
                    'type'     => 'textarea',
                    'title'    => __( 'Description', 'mixit' )
                )
            ),
        
    ));

    Redux::setSection( $mixit_option, array(
        'title'   => __( 'Call to action 1', 'mixit' ),
        'id'      => 'call_to_action_1',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'    => 'cta_1_title',
                'type'  => 'text',
                'title' => __( 'Title', 'mixit' )
            ),
            array(
                'id'    => 'cta_1_subtitle',
                'type'  => 'text',
                'title' => __( 'Sub Title', 'mixit' )
            ),
            array(
                'id'   => 'cta_1_description',
                'type' => 'textarea',
                'title' => __( 'Description', 'mixit' )
            ),
            array(
                'id'   => 'cta_1_image',
                'type' => 'media',
                'title' => __( 'Image', 'mixit' )
            ),array(
                'id'   => 'cta_1_btn_text',
                'type' => 'text',
                'title' => __( 'Button Text', 'mixit' )
            ),array(
                'id'   => 'cta_1_btn_link',
                'type' => 'text',
                'title' => __( 'Button Link', 'mixit' )
            )
        )    
    ));

    Redux::setSection( $mixit_option, array(
        'title'   => __( 'Call to action 2', 'mixit' ),
        'id'      => 'call_to_action_2',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'    => 'cta_2_title',
                'type'  => 'text',
                'title' => __( 'Title', 'mixit' )
            ),
            array(
                'id'    => 'cta_2_subtitle',
                'type'  => 'text',
                'title' => __( 'Sub Title', 'mixit' )
            ),
            array(
                'id'   => 'cta_2_description',
                'type' => 'textarea',
                'title' => __( 'Description', 'mixit' )
            ),
            array(
                'id'   => 'cta_2_image',
                'type' => 'media',
                'title' => __( 'Image', 'mixit' )
            ),array(
                'id'   => 'cta_2_btn_text',
                'type' => 'text',
                'title' => __( 'Button Text', 'mixit' )
            ),array(
                'id'   => 'cta_2_btn_link',
                'type' => 'text',
                'title' => __( 'Button Link', 'mixit' )
            )
        )    
    ));

    Redux::setSection( $mixit_option, array(
        'title' => __( 'Footer Settings', 'mixit' ),
        'id'    => 'footer',
        'icon'  => '',
        'fields'     => array(
            array(
                'id'        => 'footer_text',
                'type'      => 'text',
                'title'     => __( 'Footer Text', 'mixit' ),
                'default'   => __( 'Copyright &copy; 2016 Eaglevision IT SE', 'mixit' ),
            )
        )
    ) );
