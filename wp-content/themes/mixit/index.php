<?php get_header(); global $mixit_option; ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php get_redux_page_banner(); ?>
		<div class="post-page">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<div class="mixit-single-post-wrapper">
							<?php
								while ( have_posts() ) : the_post();
									
									if( is_home() || is_archive() ){
										get_template_part( 'template-parts/content', 'blog' );

									}else{
										get_template_part( 'template-parts/content' );

									}

									// If comments are open or we have at least one comment, load up the comment template.
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;

								endwhile; // End of the loop.
							?>

							<div class="content-page-link"> 
								<?php 
									// Previous/next page navigation.
									the_posts_pagination( array(
										'prev_text'          => __( '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>', 'mixit' ),
										'next_text'          => __( '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>', 'mixit' ),
										'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'mixit' ) . ' </span>',
									) );
								?>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<?php  get_sidebar( 'blog' ); ?>
					</div>
				</div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
