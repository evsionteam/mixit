<?php

/*action and hooks for page post type*/

if( !function_exists('add_page_metaboxes') ) :
	function add_page_metaboxes(){
		add_meta_box(
				'mixit_page_meta_box',
				__('Banner','mixit'),
				'post_meta_html',
				'page',
				'normal',
				'default'
		   );
	}
	add_action( 'add_meta_boxes', 'add_page_metaboxes' );
endif;

if( !function_exists('post_meta_html') ):
	function post_meta_html(){
		global $post;
		wp_nonce_field( basename( __FILE__ ), 'nonce_featured_page' ); ?>
		<div class="custom-options">
			<!-- Image upload starts -->
			<?php
			    $image_src = '';

			    $image_id = get_post_meta( $post->ID, '_image_id', true );
			    $image_src = wp_get_attachment_url( $image_id );

			   $image = wp_get_attachment_url( $image_id );
		   		?>
				    <div id="designer-cover-image">
							<?php if( $image) : ?>
				    	 <img src="<?php echo $image_src ?>" style="max-width:100%;" />
						 <?php endif;?>
				    </div>

				    <input type="hidden" name="upload_image_id" id="upload_image_id" value="<?php echo ($image_id)? $image_id:''; ?>" />
				    <p>
				        <a class="upload-custom-img" title="<?php esc_attr_e( 'Choose Cover image' ) ?>" href="#" id="upload-custom-img"><?php _e( 'Click to choose Cover Image', 'mixit' ) ?></a>

				        <a class="delete-custom-img" title="<?php _e( 'Remove Cover Image','mixit' ) ?>" href="#" id="delete-custom-img" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove Cover Image','mixit' ) ?></a>
				    </p>
				    <script type="text/javascript">
				        jQuery(document).ready(function($) {
				            // Set all variables to be used in scope
				             var frame,
				                 metaBox = $('.postbox-container'), // Your meta box id here
				                 addImgLink = metaBox.find('.upload-custom-img'),
				                 delImgLink = metaBox.find( '.delete-custom-img'),
				                 imgContainer = $( '#designer-cover-image'),
				                 imgIdInput = metaBox.find( '#upload_image_id' );
				             // ADD IMAGE LINK
				             addImgLink.on( 'click', function( event ){
				               event.preventDefault();
				               // If the media frame already exists, reopen it.
				               if ( frame ) {
				                 frame.open();
				                 return;
				               }
				               // Create a new media frame
				               frame = wp.media({
				                 title: 'Select or Upload Media Of Your Chosen Persuasion',
				                 button: {
				                   text: 'Use this media'
				                 },
				                 multiple: false  // Set to true to allow multiple files to be selected
				               });
				               // When an image is selected in the media frame...
				               frame.on( 'select', function() {
				                 // Get media attachment details from the frame state
				                 var attachment = frame.state().get('selection').first().toJSON();
				                 // Send the attachment URL to our custom image input field.
				                 imgContainer.html( '<img src="'+ attachment.url +'" alt="" style="max-width:100%;"/>' );
				                 // Send the attachment id to our hidden input
				                 imgIdInput.val( attachment.id );
				                 // Hide the add image link
				                 addImgLink.addClass( 'hidden' );
				                 // Unhide the remove image link
				                 delImgLink.removeClass( 'hidden' );
				               });
				               // Finally, open the modal on click
				               frame.open();
				             });
				             // DELETE IMAGE LINK
				             delImgLink.on( 'click', function( event ){
				               event.preventDefault();
				               // Clear out the preview image
				               imgContainer.html( '' );
				               // Un-hide the add image link
				               addImgLink.removeClass( 'hidden' );
				               // Hide the delete image link
				               delImgLink.addClass( 'hidden' );
				               // Delete the image id from the hidden input
				               imgIdInput.val( '' );
				             });
				        });
				    </script>
			<!-- Image upload ends -->
		</div>
		<?php
	}
endif;

if( !function_exists('mixit_save_page_option')) :
	function mixit_save_page_option(){
		global $post;

		if( "page" != $post->post_type ) {
			return $post->ID;
		}
		if ( !isset( $_POST['nonce_featured_page'] ) || !wp_verify_nonce( $_POST['nonce_featured_page'], basename( __FILE__ ) ) ){
	   		return $post->ID;
		}
		update_post_meta( $post->ID, '_image_id', $_POST['upload_image_id'] );
	}
	add_action( 'save_post', 'mixit_save_page_option', 1, 2 );
endif;
