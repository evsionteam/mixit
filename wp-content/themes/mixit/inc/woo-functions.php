<?php
	remove_action( 'woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open',10 );
	remove_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',5 );
	remove_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10 );

	remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title',10 );
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar',10 );
	function is_realy_woocommerce_page () {
        if(  function_exists ( "is_woocommerce" ) && is_woocommerce()){
                return true;
        }
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
                        return true ;
                }
        }
        return false;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'mixit_change_breadcrumb_delimiter' );
function mixit_change_breadcrumb_delimiter( $defaults ) {
        // Change the breadcrumb delimeter from '/' to '>'
        $defaults['delimiter'] = ' <i class="fa fa-chevron-right" aria-hidden="true"></i> ';
        return $defaults;
}

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

add_action( 'init', 'mixit_clear_cart_url' );
function mixit_clear_cart_url() {
        global $woocommerce;

        if ( isset( $_GET['empty-cart'] ) ) {
                $woocommerce->cart->empty_cart();
        }
}

/*ajax load products details on click*/
add_action('wp_ajax_mixit_load_product_details', 'mixit_load_product_details');
add_action('wp_ajax_nopriv_mixit_load_product_details', 'mixit_load_product_details');

function mixit_load_product_details(){
// echo get_template_directory_uri();
    $output['data'] = '';
    $product_id = $_POST['product_id'];
    ob_start();
    if(!empty($product_id)){
        $selected_product = new WP_Query( array( 'p' => $product_id, 'post_type' => 'product', 'post_per_page' => 1 ) );
        while ($selected_product->have_posts()): $selected_product->the_post();
            get_template_part( 'template-parts/wc-content-single', 'product' );
        endwhile;
        wp_reset_postdata();
    }
    $output['data'] = ob_get_clean();
    echo json_encode($output);
    die;
}
/**/

add_filter( 'woocommerce_product_description_tab_title', 'change_text_product_description_tab', 10, 2 );
function change_text_product_description_tab( $title, $key ){
    return __( 'Produktbeskrivning','mixit' );
}

add_filter( 'woocommerce_product_reviews_tab_title', 'change_text_product_reviews_tab', 10, 2 );
function change_text_product_reviews_tab( $title, $key ){
    return __( 'Omdömen','mixit' );
}

// Tabs
add_filter( 'woocommerce_product_tabs','modify_products_tabs' );
function modify_products_tabs($tabs){
    unset($tabs['additional_information']);

    $tabs['leverans'] = array(
        'title'     => __( 'Leverans', 'woocommerce' ),
        'priority'  => 40,
        'callback'  => 'woo_leverans_tab_content'
    );
    return $tabs;
}


function woo_leverans_tab_content(){
    global $post;

    $leverans = get_post_meta( $post->ID, 'leverans', true );
    if( $leverans == '' )  {
    $leverans = '<h3>Postpaket</h3>
<h3>Normalpaket</h3>
<p>Leverans inom 1-3 dagar från beställningsdatum.
Du kommer få en avi när paketet kommit fram.
Du hämtar paketet hos närmaste postombud eller det utlämningsställe du valt.
Mindre försändelser kan skickas direkt hem till din brevlåda.</p>

<h3>Hämta i butik</h3>

<p>Bor du i närheten av Sundbyberg går det bra att hämta ut din order i butiken på Sundbybergs Torg 1.</p>

<h3>Fraktavgift</h3>
<p>Fraktavgiften är bara på 69 SEK inom Sverige. Fraktavgiften kan komma ändras under reaperioder samt vid speciella erbjudanden, men då informerar vi tydligt om det först. Frakt till andra länder än Sverige följer vi en så kallad vikttariff, dvs du betalar en fraktavgift som är beroende av vikten på paketet. Se din aktuella kostnad i kassan när du gör din beställning. Vi använder Posten/PostNord som leveranspartner.
</p>
';
    update_post_meta( $post->ID, 'leverans', $leverans );
}

    if( "" != $leverans ){
        echo $leverans;
    } else {
        echo '';
    }
}

remove_action( 'woocommerce_proceed_to_checkout','woocommerce_button_proceed_to_checkout', 20 );

if( !function_exists( 'woo_add_payment_method_cart_page' ) ) :

    function woo_add_payment_method_cart_page() {
        ?></form>  <!-- form close for coupon submission -->
        <form action="<?php echo esc_url( wc_get_checkout_url() ) ;?>" method="POST">

        <?php if ( WC()->cart->needs_payment() ) : ?>
            <ul class="wc_payment_methods payment_methods methods">
                <?php
                $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
                    if ( ! empty( $available_gateways ) ) {
                        foreach ( $available_gateways as $gateway ) {
                            wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
                        }
                    } else {
                        echo '<li>' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_country() ? __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : __( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>';
                    }
                ?>
            </ul>
        <?php endif; ?>
        <button type = "submit" class="checkout-button button alt wc-forward">
            <?php echo __( 'Proceed to Checkout', 'woocommerce' ); ?>
        </button>
        <!-- form will be close automatically as whave already tag in the cart.php -->
        <?php
    }

    add_action( 'woocommerce_proceed_to_checkout', 'woo_add_payment_method_cart_page', 20 );

endif;

if ( !function_exists('woo_checkout_before_order_review') ):
    function woo_checkout_before_order_review() {
        ?>
        <input type="hidden" id="payment_method_cart" value="<?php echo isset($_POST['payment_method'])?$_POST['payment_method']:'';?>" />
        <?php
    }
    add_action('woocommerce_checkout_before_order_review', 'woo_checkout_before_order_review' );
endif;

if ( !function_exists('woo_product_categories_shortcode') ) :
    function woo_product_categories_shortcode($atts){
        global $woocommerce_loop;

        $atts = shortcode_atts( array(
            'number'     => null,
            'orderby'    => 'name',
            'order'      => 'ASC',
            'columns'    => '4',
            'hide_empty' => 1,
            'parent'     => '',
            'ids'        => ''
        ), $atts, 'product_categories' );

        $ids        = array_filter( array_map( 'trim', explode( ',', $atts['ids'] ) ) );
        $hide_empty = ( $atts['hide_empty'] == true || $atts['hide_empty'] == 1 ) ? 1 : 0;

        // get terms and workaround WP bug with parents/pad counts
        $args = array(
            'orderby'    => $atts['orderby'],
            'order'      => $atts['order'],
            'hide_empty' => $hide_empty,
            'include'    => $ids,
            'pad_counts' => true,
            'child_of'   => $atts['parent']
        );

        $product_categories = get_terms( 'product_cat', $args );

        if ( '' !== $atts['parent'] ) {
            $product_categories = wp_list_filter( $product_categories, array( 'parent' => $atts['parent'] ) );
        }

        if ( $hide_empty ) {
            foreach ( $product_categories as $key => $category ) {
                if ( $category->count == 0 ) {
                    unset( $product_categories[ $key ] );
                }
            }
        }

        if ( $atts['number'] ) {
            $product_categories = array_slice( $product_categories, 0, $atts['number'] );
        }

        $columns = absint( $atts['columns'] );
        $woocommerce_loop['columns'] = $columns;

        ob_start();

        if ( $product_categories ) {
            woocommerce_product_loop_start();

            foreach ( $product_categories as $category ) {
              ?>
              <a href="<?php echo get_term_link( $category->term_id, 'product_cat' );?>"><?php echo $category->name;?></a>
              <?php
            }

            woocommerce_product_loop_end();
        }

        woocommerce_reset_loop();

        return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';

    }

    add_shortcode('product_categories_title',  'woo_product_categories_shortcode' );
endif;
