<?php
/*action and hooks for product post type*/

if( !function_exists('add_product_metaboxes') ) :
	function add_product_metaboxes(){	

		// Products
		add_meta_box( 
			'mixit_leverans_meta_box',
			__('Leverans','mixit'),
			 'product_meta_html', 
			 'product', 
			 'normal',
			 'default'
		  );
	}
	add_action( 'add_meta_boxes', 'add_product_metaboxes' );
endif;

if( !function_exists('product_meta_html') ):
	function product_meta_html(){
		global $post;
		wp_nonce_field( basename( __FILE__ ), 'nonce_featured_product' ); 

		$settings = array(
			'textarea_name'      => 'product-leverans',
			'quicktags'          => array( 'buttons' => 'em,strong,link' ),
			'tinymce'            => array(
									'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
									'theme_advanced_buttons2' => '',
			),
			'editor_css'        => '<style>#wp-product-leverans-editor-container .wp-editor-area{height:175px; width:100%;}</style>',
			'drag_drop_upload'  => true
		);

		wp_editor( htmlspecialchars_decode( get_post_meta( $post->ID, 'leverans', true ) ), 'product-leverans', apply_filters( 'woo_product_leverans_editor_settings', $settings ) );
	}

endif;

if( !function_exists('mixit_save_woo_product_option')) :

	function mixit_save_woo_product_option(){
		global $post;
		// file_put_contents(__DIR__.'/lever.txt',print_r($post,true) );
		if( "product" != $post->post_type) {
			return $post->ID;
		}

		if ( !isset( $_POST['nonce_featured_product'] ) || !wp_verify_nonce( $_POST['nonce_featured_product'], basename( __FILE__ ) ) ){
	   		return $post->ID;
		}
		update_post_meta( $post->ID, 'leverans', $_POST['product-leverans'] );
	}

	add_action( 'save_post', 'mixit_save_woo_product_option' );

endif;

