<?php

remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_filter( 'nav_menu_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'nav_menu_item_id', 'remove_unwanted_menu_classes' );
add_filter( 'page_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'body_class', 'remove_unwanted_body_class', 10, 2 );


function remove_unwanted_menu_classes( $classes ) {
    $allowed =  array (
        'current-menu-item',
        'current-menu-ancestor',
        'menu-item-has-children',
        'current-post-ancestor',
        'first',
        'last',
        'vertical',
        'horizontal',
        'book-btn',
        'blog'
    );


    /*if ( is_array( $classes ) && is_multi_author() ) {
        $classes[] = 'group-blog';
    }*/


    // Adds a class of hfeed to non-singular pages.
    /*if (is_array( $classes ) && ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    
    if ( ! is_array( $classes ) )
        return $classes;

    foreach ( $classes as $key => $class ) {
        if ( in_array( $class, $allowed ) )
            continue;
        if ( 0 === strpos( $class, 'fa-' ) )
            continue;

        unset ( $classes[ $key ] );
    }*/

    return $classes;
}


function remove_unwanted_body_class( $wp_classes, $extra_classes ) {
    global $post;
    $whitelist = array( 'portfolio', 'home', 'error404','woocommerce', 'blog' );
    $wp_classes = array_intersect( $wp_classes, $whitelist );


    if ( isset( $post ) ) {
        $wp_classes[] = $post->post_name;
    }


    if ( is_front_page() ) {
        $wp_classes[] = 'woocommerce';
    }

    if( is_archive() ){
        $wp_classes[] = 'blog';
    }


    return array_merge( $wp_classes, (array) $extra_classes );
}


function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) ){
        $src = remove_query_arg( 'ver', $src );
    }

    return $src;
}


add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );


add_filter( 'script_loader_tag', function ( $tag, $handle ) {
    if( true == strpos($tag, "link.type = 'text/css';" ) ){
        return str_replace( "type='text/javascript'", "type='text/javascript' async", $tag );
    }
    return $tag;
}, 999, 2 );


/*
add_filter( 'style_loader_tag', function ( $tag, $handle ) {
    return str_replace( ' href', ' async href', $tag );
}, 999, 2 );*/


/* ----- Move JS to footer ------- */
/*function move_jquery_to_footer() {
    wp_deregister_script( 'jquery-migrate' );
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), null, null, true );
    wp_register_script( 'jquery-migrate', includes_url( '/js/jquery/jquery-migrate.min.js' ), array('jquery'),null, true );
    wp_enqueue_script( 'jquery-migrate' );
}

add_action( 'wp_enqueue_scripts', 'move_jquery_to_footer',9 );*/


add_filter('the_content','wpautop');
add_filter('get_the_content','wpautop');