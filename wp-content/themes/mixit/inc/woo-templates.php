<?php
function shop_side_bar(){
	if ( is_active_sidebar( 'shop-sidebar-1' ) ){
		dynamic_sidebar( 'shop-sidebar-1' );
	}
}

add_action( 'woocommerce_before_shop_loop_item' , 'mixit_before_shop_loop_item', 1 );
function mixit_before_shop_loop_item(){
	echo "<div class='product-item-inner'>";
}

add_action( 'woocommerce_after_shop_loop_item' , 'mixit_after_shop_loop_item',999 );
function mixit_after_shop_loop_item(){
	echo '</div>'; //closing product-item-info
	echo "</div>"; // product-item-inner
}

add_action( 'woocommerce_before_shop_loop_item_title' , 'mixit_before_shop_loop_item_title_start',1 );
function mixit_before_shop_loop_item_title_start(){
	global $product, $post;
	$alt_image = false;

	
	$attachment_ids = $product->get_gallery_attachment_ids();
	if(count($attachment_ids) > 0 ){
		foreach ( $attachment_ids as $attachment_id ) {
			$image = wp_get_attachment_image( $attachment_id, 'shop_catalog' ,false , array('class'=>'alt_img'));
			if($image){
				$alt_image = $image;
				break;
			}
		}
	}
?>
	<div class='product-item-image<?php echo $alt_image ? ' has-alt-image':'' ;?>'>

<?php
	echo $alt_image;
	echo '<a href="'.get_post_permalink( $post->id ).'"><div class="overlay"></div></a>';
}

add_action( 'woocommerce_before_shop_loop_item_title' , 'mixit_before_shop_loop_item_title_hover_block',9 );
function mixit_before_shop_loop_item_title_hover_block(){
	get_template_part( 'template-parts/wc-product','hover' );
}

add_action( 'woocommerce_before_shop_loop_item_title' , 'mixit_before_shop_loop_item_title_end',999 );
function mixit_before_shop_loop_item_title_end(){
	echo "</div>"; // closing product-item-image
}

add_action( 'woocommerce_shop_loop_item_title' , 'mixit_shop_loop_item_title_start',1 );
function mixit_shop_loop_item_title_start(){
	echo "<div class='product-item-info'>";
}

add_filter( 'post_class', 'mixit_post_class' );
function mixit_post_class( $classes ) {
	//var_dump( $classes ); die();
	if( is_product() ){
		$classes[] = 'product-single-wrapper';
	}
   
    return $classes;
}

add_action( 'woocommerce_shop_loop_item_title', 'mixit_shop_loop_item_title', 10 );
function mixit_shop_loop_item_title() {
	$title = get_the_title();
	echo '<h3><a data-toggle="tooltip" data-placement="top" title="'.$title.'" href="'.get_the_permalink().'" >' . $title . '</a></h3>';
}

add_action( 'woocommerce_before_main_content', 'mixit_before_main_content', 11 );
function mixit_before_main_content(){
	global $post;
	$shop_page_id = wc_get_page_id( 'shop' );
	$image = get_the_post_thumbnail_url($shop_page_id, 'large');

	$title = get_the_title( $post->ID );
	

?>
	<?php if( $image ): ?>
		<style>
			.page-banner-image{ background-image: url("<?php echo $image; ?>"); }
		</style>
	<?php endif; ?>

	<div class="page-banner-image <?php echo ( ! $image ) ? 'no-image' : '';  ?>">  
		<div class="banner-centered-title-header">
			<h2 class="banner-centered-title" >
			<?php 
				if( is_woocommerce() ){
					woocommerce_page_title();
				}else{
					echo $title;
				}
				  ?>
					
			</h2>
		</div>
<?php
		if( $image ){
			?>
				<img class="hidden-xs" src="<?php echo $image; ?>" width="100%" alt="<?php _e( 'Header Image','mixit' ); ?>" />
			<?php
		}
		echo '<div class="breadcrumb-holder">';
}

add_action( 'woocommerce_before_main_content', 'mixit_before_main_content_2', 99 );
function mixit_before_main_content_2(){
	echo '</div>'; // //closing-breadcrumb-holder
	echo '</div>'; // closing page-banner-image
}

// Hiding Default page Title
add_filter( 'woocommerce_show_page_title', 'mixit_show_page_title' );
function mixit_show_page_title(){
	return false;
}

//Wrapping up all products
add_action( 'woocommerce_before_shop_loop', 'mixit_before_shop_loop_1',1  );
function mixit_before_shop_loop_1(){
	echo "<div class='shop'>";
	echo "<div class='shop-sidebar col-md-3 hidden-xs'>";
}

add_action( 'woocommerce_after_shop_loop', 'mixit_after_shop_loop',99 );
function mixit_after_shop_loop(){
	echo '</div>'; //closing product wrapper
	echo '</div>'; //closing shop-right-col
	echo '</div>'; //closing shop
}

add_action( 'woocommerce_before_shop_loop', 'mixit_after_shop_loop_2',19 );
function mixit_after_shop_loop_2(){
	$shop_page_id = wc_get_page_id( 'shop' );
	shop_side_bar();
	echo '</div>'; //closing shop-sidebar
	echo '<div class="shop-right-col col-md-9">';
	echo '<div class="mixit-shop-toolbar">';
	?>
	<a href="<?php echo get_permalink($shop_page_id) ?>">All</a>
	<a href="" class="visible-xs" id="filtertoggle">Filter <i class="fa fa-filter"></i></a>
	<?php
}

add_action( 'woocommerce_before_shop_loop', 'mixit_after_shop_loop_3',31 );
function mixit_after_shop_loop_3(){

	echo '</div>'; //closing .shop-filter-wrapper
	echo '</div>'; //closing mixit-shop-toolbar
	echo '<div class="product-wrapper">';
}

add_action( 'woocommerce_before_shop_loop_item_title', 'mixit_add_badge_new', 15 );
function mixit_add_badge_new(){
	global $post;
	$post_date = get_the_time( 'Y-m-d', $post );
	$post_date_stamp = strtotime( $post_date );
	$newness = 3000;
	if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $post_date_stamp ) {
		$class = 'badge new-badge';
		echo '<span class="' . $class . '">' . esc_html__( 'New', 'riven' ) . '</span>';
	}
}

add_filter( 'woocommerce_sale_flash', 'mixit_add_badge_sale', 20, 3 );
function mixit_add_badge_sale($return, $post, $product){
		$sale = 0;
		$return = '<span class="badge onsale">';
		if ( $product->product_type == 'variable' ) {

			$prices = $product->get_variation_prices( true );
			if ( $prices ) {
				$min_price = current( $prices[ 'price' ] );
				$max_price = end( $prices[ 'price' ] );
				if ( $max_price > 0 ) {
					$sale = ( $max_price - $min_price ) / $max_price;
				}
			}

		} else {
			if($product->get_regular_price()){
				$sale = ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price();
			}else{
				$sale = 0;
			}
		}

		if ( $sale > 0 && $sale < 1 ) {
			$return .= '-' . round( ( $sale * 100 ) ) . '%';
		} else {
			$return .= esc_attr__( 'Sale!', 'riven' );
		}
		$return .= '</span>';

		return $return;
}

add_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_discount', 9 );
function woocommerce_cart_discount(){

	echo "<div class='col-md-6'>";

	if ( wc_coupons_enabled() ) { ?>
		<div class="coupon">

			<h2 class="discount-title">
				<?php _e( 'Coupon', 'woocommerce' ); ?>
			</h2>
			<div class="discount-description">
				<em><?php _e( 'Enter your coupon code if you have one.', 'mixit' ); ?></em>
			</div>
			<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> 
			<input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

			<?php do_action( 'woocommerce_cart_coupon' ); ?>
		</div>
	<?php } ?>


	<?php do_action( 'woocommerce_cart_actions' ); ?>

	<?php wp_nonce_field( 'woocommerce-cart' ); 
	echo '</div>';
}


add_action( 'woocommerce_after_cart_table', 'mixit_after_cart_table' );
function mixit_after_cart_table(){
	global $woocommerce;
	$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
	?>
	<div class="mixit-cart-buttons-wrapper">
		<div class="mixit-cart-buttons-left">
			<a class="button cart-table-btn" 
				href="<?php echo $shop_page_url; ?>">
				<?php _e( 'CONTINUE SHOPPING', 'mixit' ); ?>
			</a>	
		</div>
		<div class="mixit-cart-buttons-right">
		<input type="hidden" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'mixit' ); ?>" />
		<button type="submit" 
				class="button cart-table-btn" 
				 ><?php esc_attr_e( 'Update Cart', 'mixit' ); ?></button>
			

			<a class="button cart-table-btn" href="<?php echo $woocommerce->cart->get_cart_url(); ?>?empty-cart">
				<?php _e( 'Empty Cart', 'mixit' ); ?>
			</a>
		</div>
	</div>
	<?php

}

add_action( 'woocommerce_cart_is_empty', 'mixit_cart_is_empty' );
function mixit_cart_is_empty(){
	?>
	<div class="cart-empty-wrapper text-center">
		<p><i class="fa fa-shopping-cart" aria-hidden="true"></i></p>
		<h2><?php _e( 'YOUR CART IS CURRENTLY EMPTY.', 'mixit' ); ?></h2>
		<p class="empty-para"><?php _e( "DON'T WORRY ITS NOT TOO LATE TO BUY SOMETHING",'mixit') ?></p>
		<?php if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
			<p class="return-to-shop">
				<a class="button wc-backward" 
					href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
					<?php _e( 'Return To Shop', 'woocommerce' ) ?>
				</a>
			</p>
		<?php endif; ?>
	</div>
	<?php
}

add_action( 'woocommerce_before_shop_loop', 'mixit_before_shop_loop1', 19 );
function mixit_before_shop_loop1(){
	echo "<div class='shop-filter-wrapper'>";
}


add_filter( 'woocommerce_product_description_heading', 'mixit_product_description_heading', 11 );

function mixit_product_description_heading( $heading ){
	return false;
}

add_filter( 'woocommerce_output_related_products_args', 'mixit_output_related_products_args' );
function mixit_output_related_products_args( $args ){

	$args['posts_per_page'] = 5;
	$args['columns'] = 5;
	return $args;
}

add_action( 'woocommerce_before_single_product_summary', 'mixit_before_single_product_summary',9 );
function mixit_before_single_product_summary(){
	echo "<div class='product-single-holder'>";
}

add_action( 'woocommerce_after_single_product_summary', 'mixit_after_single_product_summary',16 );
function mixit_after_single_product_summary(){
	echo "</div>"; //closing of product-single-holder
}

add_filter( 'woocommerce_product_review_comment_form_args', 'mixit_woocommerce_product_review_comment_form_args' );
function mixit_woocommerce_product_review_comment_form_args( $comment_form ){

	$commenter = wp_get_current_commenter();

	$post_id = get_the_ID();

	if( wp_count_comments( $post_id ) > 0 ){
		$comment_form[ 'title_reply' ] = 'Be the first to review "'. get_the_title().'"';
	}

	$comment_form[ 'title_reply_before' ] = '<h2  class="comment-reply-title">';
	$comment_form[ 'title_reply_after' ] = '</h2>';
	$comment_form[ 'fields' ] = array(
					'author' => '<p class="review-input comment-form-author"><input placeholder="'.__( 'Name', 'mixit' ).'" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
					'email'  => '<p class="review-input comment-form-email"><input placeholder="'.__( 'Email','mixit' ).'" id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>',
				);
	$comment_form[ 'comment_notes_before' ] = '';
	$comment_form['comment_field']  = '<p class="comment-form-comment"><textarea placeholder="'._x( 'Comment', 'mixit' ).'" id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>';
	
	return $comment_form;
}	

add_filter( 'wc_add_to_cart_params', 'mixit_add_to_cart_params' );
function mixit_add_to_cart_params( $params ){
	$params['i18n_view_cart'] = "<i class='fa fa-eye' aria-hidden='true'></i>";
	return $params;
}

add_action( 'woocommerce_share', 'mixit_all_share' );
function mixit_all_share(){
	echo do_shortcode( '[wp_allshare theme="theme4" networks="facebook,twitter,linkedin,gplus,pinterest,digg" /]' );
} 

//add_filter( 'woocommerce_stock_html', 'modify_stock_text', 10, 3 );
function modify_stock_text( $availability_html, $availability, $product ){

	$availability      = $product->get_availability();

	if( $product->is_in_stock() ){
		$prefix = '<i class="fa fa-check"></i>';
	}else{
		$prefix = '<i class="fa fa-close"></i>';
	}

	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . $prefix . esc_html( $availability['availability'] ) . '</p>';

	return $availability_html;
}

add_action( 'woocommerce_single_product_summary', 'add_stock_text', 19 );
function add_stock_text(){

	global $product;
	$availability      = $product->get_availability();
	
	if( $product->is_in_stock() ){
		$prefix = '<i class="fa fa-check"></i>';
	}else{
		$prefix = '<i class="fa fa-close"></i>';
	}

	echo $availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . $prefix . esc_html( $availability['availability'] ) . '</p>';

}

add_action('woocommerce_single_product_summary','show_product_attributes',25);

function show_product_attributes(){
	global $product;
	
	$product->list_attributes();	
}