<?php
function get_new_class_before_widget( $args, $new_class = 'newclass' ){
	//return $args['before_widget'];
	if( strpos($args['before_widget'], 'class') === false ) {
		$args['before_widget'] = str_replace('>', 'class="'. $new_class . '"', $args['before_widget']);
	}
	// there is 'class' attribute - append width value to it
	else {
		$args['before_widget'] = str_replace('class="', 'class="'. $new_class . ' ', $args['before_widget']);
	}

	return $args['before_widget'];
}
class Mixit_Contact extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'contact_widget', // Base ID
			__( 'Contacts', 'mixit' ), // Name
			array( 'description' => __( 'Widget for Footer contact', 'mixit' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<div class="contact-information">
			<div class="contact-item">
				<div class="contact-item-icon"><i class="fa fa-location-arrow"></i></div>
				<div class="contact-item-content text-uppercase letter-spacing-1">
					<?php echo $instance[ 'address' ]; ?>
				</div>
			</div>
			<div class="contact-item">
				<div class="contact-item-icon"><i class="fa fa-phone"></i></div>
				<div class="contact-item-content"><?php echo $instance[ 'phone' ]; ?></div>
			</div>
			<div class="contact-item">
				<div class="contact-item-icon"><i class="fa fa-envelope"></i></div>
				<div class="contact-item-content">
					<a href="mailto:<?php echo $instance[ 'email' ]; ?>">
						<?php echo $instance[ 'email' ]; ?>
					</a>
				</div>
			</div>
			<div class="contact-item">
				<div class="contact-item-icon"><i class="fa fa-clock-o"></i></div>
				<div class="contact-item-content"><?php echo $instance[ 'time'  ]; ?></div>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'mixit' );
		$address = ! empty( $instance['address'] ) ? $instance['address'] : '';
		$phone = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
		$email = ! empty( $instance['email'] ) ? $instance['email'] : '';
		$time = ! empty( $instance['time'] ) ? $instance['time'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>">
				<?php _e( esc_attr( 'Address:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" 
				value="<?php echo esc_attr( $address ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>">
				<?php _e( esc_attr( 'Phone:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" 
				value="<?php echo esc_attr( $phone ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>">
				<?php _e( esc_attr( 'Email:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" 
				value="<?php echo esc_attr( $email ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'time' ) ); ?>">
				<?php _e( esc_attr( 'Time:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'time' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'time' ) ); ?>" 
				value="<?php echo esc_attr( $time ); ?>" />
		</p>

		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
		$instance['phone']   = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
		$instance['email']   = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
		$instance['time']    = ( ! empty( $new_instance['time'] ) ) ? strip_tags( $new_instance['time'] ) : '';
		
		return $instance;
	}
}

class Mixit_text_with_button extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'text_with_button', // Base ID
			__( 'Text With Button', 'mixit' ), // Name
			array( 'description' => __( 'Arbitrary text or HTML with a button.', 'mixit' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo get_new_class_before_widget( $args, 'customer' );
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<div class="mixit-text-button-wrapper">
			<?php echo $instance['content']; ?>
			<div class="mixit-text-button">
				<a href="<?php echo $instance[ 'btn_link' ] ?>">
					<?php echo $instance['btn_text']; ?>
				</a>
			</div>
		</div> 
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'mixit' );
		$content = ! empty( $instance['content'] ) ? $instance['content'] : '';
		$btn_link = ! empty( $instance['btn_link'] ) ? $instance['btn_link'] : '';
		$btn_text = ! empty( $instance['btn_text'] ) ? $instance['btn_text'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>">
				<?php _e( esc_attr( 'Content:' ) ); ?>
			</label>
			<br>
			<textarea class="widefat" 
				id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" ><?php echo $content; ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'btn_text' ) ); ?>">
				<?php _e( esc_attr( 'Button Text:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'btn_text' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'btn_text' ) ); ?>" 
				value="<?php echo esc_attr( $btn_text ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'btn_link' ) ); ?>">
				<?php _e( esc_attr( 'Button Link:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'btn_link' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'btn_link' ) ); ?>" 
				value="<?php echo esc_attr( $btn_link ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['content'] = ( ! empty( $new_instance['content'] ) ) ? strip_tags( $new_instance['content'] ) : '';
		$instance['btn_text']= ( ! empty( $new_instance['btn_text'] ) ) ? strip_tags( $new_instance['btn_text'] ) : '';
		$instance['btn_link']   = ( ! empty( $new_instance['btn_link'] ) ) ? strip_tags( $new_instance['btn_link'] ) : '';
		
		return $instance;
	}
}

class Mixit_testimonial extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'mixit-testimonial', // Base ID
			__( 'Testimonial', 'mixit' ), // Name
			array( 'description' => __( 'Testimonial widget.', 'mixit' ), ) // Args
		);

		add_action( 'wp_enqueue_scripts',array( $this,'enqueue_script' ) );
	}

	public function enqueue_script(){
		$script = "
				   jQuery( window ).load(function(){
					jQuery('#mixit-testimonial-carou').caroufredsel({
						responsive: true,
						scroll: {
							duration : 800
						},
						swipe       : {
							onTouch : true,
							onMouse : true
						},
					});
				});
			";
    	wp_add_inline_script( 'mixit-script',$script );


    	$styles = ".testimonial-wrapper{
				overflow: hidden;
			}

			.mixit-testimonial-item{
				float: left;
			}

			.caroufredsel_wrapper{
				cursor: pointer !important;
			}";

			wp_add_inline_style( 'mixit-style', $styles );
	}



	public function widget( $args, $instance ) {

		echo get_new_class_before_widget( $args, 'customer-review' );
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		?>
		<div class="testimonial-wrapper">
			<div id="mixit-testimonial-carou">
			<?php
			$param = array( 'post_type' => 'testimonial', 'posts_per_page' => -1 );
			$testimonials = get_posts( $param );
			foreach ( $testimonials as $post ) : 
			?>
				<div class="mixit-testimonial-item text-center">

					<?php if( has_post_thumbnail($post->ID)): ?>
						<?php echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'img-responsive' ) ); ?>
					<?php endif; ?>
					
					<div class="testimonial-inner-wrapper">
					<div class="testimonial-content">
						<?php echo wp_trim_words( $post->post_content,32, $more = '...' );  ?>
					</div>
					<h3 class="mixit-testimonial-title">
						<?php echo $post->post_title;  ?>
						<!-- <a href="<?php echo get_permalink($post->ID); ?>"> -->
							
						<!-- </a> -->
					</h3>
 					</div><!-- /testimonial-inner-wrapper -->
				</div>
			<?php
			endforeach;
			?>
			</div>
		</div> 
			<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'mixit' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		return $instance;
	}
}

class Mixit_subscription extends WP_Widget {

	function __construct() {

		parent::__construct(
			'mixit-subscription', // Base ID
			__( 'Subscription', 'mixit' ), // Name
			array( 'description' => __( 'Subscription widget.', 'mixit' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo get_new_class_before_widget( $args, 'customer-offer' );

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<div class="customer-offer">
			<p>
				<?php echo $instance['description']; ?>
			</p>
			<?php echo do_shortcode( $instance['shortcode'] ); ?>
		</div> 
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'mixit' );
		$description = ! empty( $instance['description'] ) ? $instance['description'] : __( '', 'mixit' );
		$shortcode = ! empty( $instance['shortcode'] ) ? $instance['shortcode'] : __( '', 'mixit' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>">
				<?php _e( esc_attr( 'Description:' ) ); ?>
			</label> 
			<br>
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" ><?php echo esc_attr( $description ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'shortcode' ) ); ?>">
				<?php _e( esc_attr( 'Contact Form 7 Shortcode:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'shortcode' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'shortcode' ) ); ?>" 
				value="<?php echo esc_attr( $shortcode ); ?>" />
		</p>
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description']   = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		$instance['shortcode']   = ( ! empty( $new_instance['shortcode'] ) ) ? strip_tags( $new_instance['shortcode'] ) : '';
		
		return $instance;
	}
}

class Mixit_brand_list extends WP_Widget {

	function __construct() {

		parent::__construct(
			'mixit-brand-list', // Base ID
			__( 'Brand', 'mixit' ), // Name
			array( 'description' => __( 'Brand Lists.', 'mixit' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo get_new_class_before_widget( $args, 'brand-list-wrapper' );

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

	
		$terms = get_terms( array(
		    'taxonomy' => 'brand',
		    'hide_empty' => true,
		) );
		if( $terms ):
		?>
		<ul class="brand-list">
			<?php foreach( $terms as $term ): ?>
				<li><a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo $term->name; ?></a></li>
			<?php endforeach; ?>
		</ul> 
		<?php
		endif;
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'mixit' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		return $instance;
	}
}

// register Foo_Widget widget
function register_contact() {
    register_widget( 'Mixit_Contact' );
    register_widget( 'Mixit_text_with_button' );
    register_widget( 'Mixit_testimonial' );
    register_widget( 'Mixit_subscription' );
    register_widget( 'Mixit_brand_list' );
}

add_action( 'widgets_init', 'register_contact' );

