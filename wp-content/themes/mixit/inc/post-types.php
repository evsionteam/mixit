<?php

function mixit_register_post_type(){
	$labels = array(
		'name'               => _x( 'Designer', 'post type general name', 'mixit' ),
		'singular_name'      => _x( 'Personalens modeplock', 'post type singular name', 'mixit' ),
		'menu_name'          => _x( 'Designer', 'admin menu', 'mixit' ),
		'name_admin_bar'     => _x( 'Designer', 'add new on admin bar', 'mixit' ),
		'add_new'            => _x( 'Add New', 'Designer', 'mixit' ),
		'add_new_item'       => __( 'Add New Designer', 'mixit' ),
		'new_item'           => __( 'New Designer', 'mixit' ),
		'edit_item'          => __( 'Edit Designer', 'mixit' ),
		'view_item'          => __( 'View Designer', 'mixit' ),
		'all_items'          => __( 'All Designer', 'mixit' ),
		'search_items'       => __( 'Search Designer', 'mixit' ),
		'parent_item_colon'  => __( 'Parent Designer:', 'mixit' ),
		'not_found'          => __( 'No Designer found.', 'mixit' ),
		'not_found_in_trash' => __( 'No Designer found in Trash.', 'mixit' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'mixit' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'rewrite'			 => array( 'slug' => 'personal' ),
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);
	register_post_type( 'designer', $args );
}
add_action( 'init', 'mixit_register_post_type' );


function add_designer_metaboxes(){
	add_meta_box('mixit_designer_meta_box', 'Below Banner', 'designer_meta_html', 'designer', 'normal', 'default');
}
add_action( 'add_meta_boxes', 'add_designer_metaboxes' );

function designer_meta_html(){
	global $post;
	$featured_id = get_option( 'featured_designer' );
	$title_below_banner = get_post_meta( $post->ID, 'title_below_banner', true );
	$description_below_banner = get_post_meta( $post->ID, 'description_below_banner', true );
	?>
	<?php wp_nonce_field( basename( __FILE__ ), 'nonce_featured_designer' ); ?>
	<style>
		.custom-options input[type = 'text'],
		.custom-options textarea{
			width:100%;
			padding:7px 10px;
		}

		.custom-options textarea{
			height: 150px;
		}

		.custom-options label{
			font-weight: bold;
		}

		.custom-options table td{
			padding: 7px;
		}
	</style>
	<div class="custom-options">
		<table width="100%">
			<tr>
				<td><label><?php _e( 'Title','mixit' ); ?> : </label></td>
				<td><input
					type="text"
					name="title_below_banner"
					class=""
					autocomplete="off"
					value="<?php echo $title_below_banner ?>"></td>
			</tr>
			<tr>
				<td>
					<label><?php _e( 'Description','mixit' ); ?> : </label>
				</td>
				<td>
					<textarea name="description_below_banner" class=""><?php echo $description_below_banner ?></textarea>
				</td>
			</tr>
		</table>

		<p>
			<input id="designer-metabox" type="checkbox"
			name="featured_designer"
			value="featured"
			<?php checked( $featured_id, $post->ID, true ); ?>>
			<label for="designer-metabox"><?php _e( 'Make it featured.', 'mixit' ); ?></label>
		</p>

		<!-- Image upload starts -->
		<?php
		    $image_src = '';

		    $image_id = get_post_meta( $post->ID, '_image_id', true );
		    $image_src = wp_get_attachment_url( $image_id );

		   $image = wp_get_attachment_url( $image_id );
	   		?>
			    <div id="designer-cover-image">
						<?php	if( $image) : ?>
							<img src="<?php echo $image_src ?>" style="max-width:100%;" />
						<?php endif;?>
			    </div>
			    <input type="hidden" name="upload_image_id" id="upload_image_id" value="<?php echo ($image_id)? $image_id:''; ?>" />
			    <p>
						<a class="upload-custom-img" title="<?php esc_attr_e( 'Choose Cover image' ) ?>" href="#" id="upload-custom-img"><?php _e( 'Click to choose Cover Image', 'mixit' ) ?></a>
			        <a class="delete-custom-img" title="<?php _e( 'Remove Cover Image','mixit' ) ?>" href="#" id="delete-custom-img" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove Cover Image','mixit' ) ?></a>
			    </p>
			    <script type="text/javascript">
			        jQuery(document).ready(function($) {
			            // Set all variables to be used in scope
			             var frame,
			                 metaBox = $('.postbox-container'), // Your meta box id here
			                 addImgLink = metaBox.find('.upload-custom-img'),
			                 delImgLink = metaBox.find( '.delete-custom-img'),
			                 imgContainer = $( '#designer-cover-image'),
			                 imgIdInput = metaBox.find( '#upload_image_id' );


			             // ADD IMAGE LINK
			             addImgLink.on( 'click', function( event ){
			               event.preventDefault();
			               // If the media frame already exists, reopen it.
			               if ( frame ) {
			                 frame.open();
			                 return;
			               }

			               // Create a new media frame
			               frame = wp.media({
			                 title: 'Select or Upload Media Of Your Chosen Persuasion',
			                 button: {
			                   text: 'Use this media'
			                 },
			                 multiple: false  // Set to true to allow multiple files to be selected
			               });


			               // When an image is selected in the media frame...
			               frame.on( 'select', function() {
			                 // Get media attachment details from the frame state
			                 var attachment = frame.state().get('selection').first().toJSON();

			                 // Send the attachment URL to our custom image input field.
			                 imgContainer.html( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );

			                 // Send the attachment id to our hidden input
			                 imgIdInput.val( attachment.id );

			                 // Hide the add image link
			                 // addImgLink.addClass( 'hidden' );

			                 // Unhide the remove image link
			                 delImgLink.removeClass( 'hidden' );
			               });

			               // Finally, open the modal on click
			               frame.open();
			             });


			             // DELETE IMAGE LINK
			             delImgLink.on( 'click', function( event ){
			               event.preventDefault();
			               // Clear out the preview image
			               imgContainer.html( '' );
			               // Un-hide the add image link
			               addImgLink.removeClass( 'hidden' );
			               // Hide the delete image link
			               delImgLink.addClass( 'hidden' );
			               // Delete the image id from the hidden input
			               imgIdInput.val( '' );
			             });


			        });
			    </script>
		<!-- Image upload ends -->
	</div>
	<?php
}

function mixit_save_designer_option(){
	global $post;

	if( "designer" != $post->post_type ){
		return $post->ID;
	}
	

	if ( !isset( $_POST['nonce_featured_designer'] ) || !wp_verify_nonce( $_POST['nonce_featured_designer'], basename( __FILE__ ) ) ){
   		return $post->ID;
	}

	if( isset( $_POST['featured_designer'] ) ){
		update_option( 'featured_designer', $post->ID );
	}
	file_put_contents(__DIR__.'/lever.txt',$_POST['description_below_banner'] );

	update_post_meta( $post->ID, 'title_below_banner', $_POST['title_below_banner'] );
	update_post_meta( $post->ID, 'description_below_banner', $_POST['description_below_banner'] );
	update_post_meta( $post->ID, '_image_id', $_POST['upload_image_id'] );
}
add_action( 'save_post', 'mixit_save_designer_option', 1, 2 );
