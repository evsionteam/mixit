<?php

/* Woocommerce Brand*/

if( !function_exists('mixit_register_taxonomy') ):

	function mixit_register_taxonomy(){

		$labels = array(
			'name'              => _x( 'Brand', 'Brand', 'mixit' ),
			'singular_name'     => _x( 'Brand', 'Brand', 'mixit' ),
			'search_items'      => __( 'Search Brands', 'mixit' ),
			'all_items'         => __( 'All Brands', 'mixit' ),
			'parent_item'       => __( 'Parent Brand', 'mixit' ),
			'parent_item_colon' => __( 'Parent Brand:', 'mixit' ),
			'edit_item'         => __( 'Edit Brand', 'mixit' ),
			'update_item'       => __( 'Update Brand', 'mixit' ),
			'add_new_item'      => __( 'Add New Brand', 'mixit' ),
			'new_item_name'     => __( 'New Brand Name', 'mixit' ),
			'menu_name'         => __( 'Brand', 'mixit' ),
		);
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'brand' ),
		);
		register_taxonomy( 'brand', 'product', apply_filters('mixit_woo_brand_arguments',$args ) );
	}

	add_action( 'init', 'mixit_register_taxonomy' );
endif;


/* Add Image Upload to Series Taxonomy */
// Add Upload fields to "Add New Taxonomy" form
if( !function_exists('add_brand_tax_image_field') ):
	function add_brand_tax_image_field() {
		// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="target"><?php _e('Target','mixit');?></label>
		<select id="target" name="brand_target">
			<option value=""><?php _e('Choose','mixit');?></option>
			<option value="_blank"><?php _e('Blank','mixit');?></option>
			<option value="_self"><?php _e('Self','mixit');?></option>
			<option value="_new"><?php _e('New','mixit');?></option>
		</select>
	</div>

		<div class="form-field">

			<label for="series_image"><?php _e( 'Brand Image:', 'mixit' ); ?></label>
			<input type="text" name="series_image[image]" id="series_image[image]" class="series-image" value="<?php echo $seriesimage; ?>">
			<input class="upload_image_button button" name="_add_series_image" id="_add_series_image" type="button" value="<?php _e('Select/Upload Image','mixit');?>" />
			<script>
				jQuery(document).ready(function() {
					jQuery('#_add_series_image').click(function() {
						wp.media.editor.send.attachment = function(props, attachment) {
							jQuery('.series-image').val(attachment.url);
						}
						wp.media.editor.open(this);
						return false;
					});
				});
			</script>
		</div>
	<?php
	}
	add_action( 'brand_add_form_fields', 'add_brand_tax_image_field', 10, 2 );
endif;

if( !function_exists('brand_edit_meta_field') ):
	// Add Upload fields to "Edit Taxonomy" form
	function brand_edit_meta_field($term) {

		// put the term ID into a variable
		$t_id = $term->term_id;

		// retrieve the existing value(s) for this meta field. This returns an array
		$term_meta = get_option( "brand_$t_id" ); 

		$target = get_term_meta( $t_id , 'target', true );		
		?>
		<tr class="form-field">
			<th for="target"><?php _e('Target','mixit');?></th>
			<td>
				<select id="target" name="brand_target">
					<option value=""><?php _e('Choose','mixit');?></option>
					<option value="_blank" <?php selected( '_blank', $target );?>><?php _e('Blank','mixit');?></option>
					<option value="_self" <?php selected( '_self', $target );?>><?php _e('Self','mixit');?></option>
					<option value="_new" <?php selected( '_new', $target );?>><?php _e('New','mixit');?></option>
				</select>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="_series_image"><?php _e( 'Brand Image', 'mixit' ); ?></label></th>
			<td>
				<?php
					$seriesimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : '';
					?>
				<input type="text" name="series_image[image]" id="series_image[image]" class="series-image" value="<?php echo $seriesimage; ?>">
				<input class="upload_image_button button" name="_series_image" id="_series_image" type="button" value="Select/Upload Image" />
			</td>
		</tr>
		<tr class="form-field">
		<th scope="row" valign="top"></th>
			<td style="height: 150px;">
				<style>
					div.img-wrap {
						background: url('http://placehold.it/960x300') no-repeat center;
						background-size:contain;
						max-width: 450px;
						max-height: 150px;
						width: 100%;
						height: 100%;
						overflow:hidden;
					}
					div.img-wrap img {
						max-width: 450px;
					}
				</style>
				<div class="img-wrap">
					<img src="<?php echo $seriesimage; ?>" id="series-img">
				</div>
				<script>
					'use strict';

					jQuery(document).ready(function() {
						jQuery('#_series_image').click(function() {
							wp.media.editor.send.attachment = function(props, attachment) {
								jQuery('#series-img').attr("src",attachment.url);
								jQuery('.series-image').val(attachment.url);
							}
							wp.media.editor.open(this);
							return false;
						});
					});
				</script>
			</td>
		</tr>
	<?php
	}
add_action( 'brand_edit_form_fields', 'brand_edit_meta_field', 10, 2 );
endif;

if ( !function_exists('save_brand_custom_meta') ):

	// Save Taxonomy Image fields callback function.
	function save_brand_custom_meta( $term_id ) {
		if ( isset( $_POST['series_image'] ) ) {
			$t_id = $term_id;
			$term_meta = get_option( "brand_$t_id" );
			$cat_keys = array_keys( $_POST['series_image'] );
			foreach ( $cat_keys as $key ) {
				if ( isset ( $_POST['series_image'][$key] ) ) {
					$term_meta[$key] = $_POST['series_image'][$key];
				}
			}
			// Save the option array.
			update_option( "brand_$t_id", $term_meta );
		}

		if( isset( $_POST['brand_target'] ) ) {
			// echo 'asdf'; die;
			update_term_meta( $term_id, $meta_key='target', $_POST['brand_target']  );
		}
	}
	add_action( 'edited_brand', 'save_brand_custom_meta', 10, 2 );
	add_action( 'create_brand', 'save_brand_custom_meta', 10, 2 );
endif;

