module.exports = function( grunt ){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		makepot: {
			target: {
				options: {
					mainFile:'./style.css',
					type: 'wp-theme',
					domainPath:'./languages/'
				}
			}
		},
		sass: {                              // Task
		    dist: {                            // Target
		      options: {                       // Target options
		      	style: 'expanded'
		      },
		      files: {                         // Dictionary of files
		        'assets/src/css/main.css': './assets/src/sass/main.scss',       // 'destination': 'source'
		        'style.css' : './assets/src/sass/style.scss'
		    	}
			}
		},
		concat: {
			options:{
				separator:"\n /*** New File ***/ \n"
			},
			/* Added new JS here*/	
			js: { 
				src: [ 
				'./assets/src/js/wrapper/start.js',
				'./assets/src/js/skip-link-focus-fix.js',
				'./node_modules/bootstrap/dist/js/bootstrap.js',
				'./node_modules/isotope-layout/dist/isotope.pkgd.js',
				'./assets/src/js/jquery.touchSwipe.js',
				'./assets/src/js/jquery.carouFredSel-6.2.1-packed.js',
				'./assets/src/js/easyzoom.js',
				'./node_modules/jquery.mmenu/dist/js/jquery.mmenu.min.js',
				'./assets/src/js/jquery.nicescroll.min.js',
				'./assets/src/js/script.js',
				'./assets/src/js/wrapper/end.js'
				],
				dest: './assets/build/js/script.js'
			},
			/* Add CSS here*/
			css: {
				src: [ 
				'./node_modules/bootstrap/dist/css/bootstrap.css',
				'./node_modules/font-awesome/css/font-awesome.css',
				'./node_modules/jquery.mmenu/dist/css/jquery.mmenu.css',
				'./assets/src/css/*.css'
				],
				dest: './assets/build/css/main.css'
			}
		},
		uglify: {
			options: {
				report: 'gzip'
			},
			main: {
				src: ['./assets/build/js/script.js'],
				dest: './assets/build/js/script.min.js'
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments : 0
			},
			target: {
				files: [{
					expand: true,
					cwd: './assets/build/css/',
					src: ['*.css', '!*.min.css'],
					dest: './assets/build/css/',
					ext: '.min.css'
				}]
			}
		},

		copy: {
			main: {
		    /*files: [
		      {expand: false, flatten: true, src: ['./node_modules/bootstrap/dist/fonts/*','./node_modules/font-awesome/fonts/*'], dest: './assets/build/fonts/', filter: 'isFile'},
		      {expand: false, flatten: true, src: ['./assets/src/images/*'], dest: './assets/src/images/', filter: 'isFile'},
		      ],*/

		      files: [
		      { expand: true, cwd: './assets/src/images', src: '**', dest: './assets/build/img/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './node_modules/bootstrap/dist/fonts', src: '**', dest: './assets/build/fonts/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './node_modules/font-awesome/fonts', src: '**', dest: './assets/build/fonts/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './assets/src/fonts', src: '**', dest: './assets/build/fonts/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './assets/src/video', src: '**', dest: './assets/build/video/', filter: 'isFile'},
		      { expand: true, flatten: true,  cwd: './assets/src/vendor-css', src: '**', dest: './assets/src/css/', filter: 'isFile'},
		      ],
		  },
		},
		
		watch: {
			js : {
				files : ['./assets/src/js/*.js'],
				tasks : [ 'js' ]
			},
			css : {
				files : ['./assets/src/sass/**/*.scss'],
				tasks : [ 'css']
			},
			php : {
				files : ['*.php','**/*.php'],
				task  : [ 'makepot' ]
			}
		} 
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-wp-i18n');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-sass');

    //register grunt default task
    grunt.registerTask( 'css', [ 'sass', 'concat', 'cssmin'] );
    grunt.registerTask( 'js', [ 'concat', 'uglify' ] );
    // grunt.registerTask( 'copy', ['copy'] );
    grunt.registerTask( 'makepot', ['makepot'] );

    grunt.registerTask('default', ['copy', 'sass', 'concat','uglify','cssmin'] );
}